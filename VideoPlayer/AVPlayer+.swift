//
//  AVPlayer.swift
//
//  Created by Vitaly Trofimov on 13.05.2021.
//  Copyright © 2021 Tachos. All rights reserved.
//

import UIKit
import AVFoundation



extension AVPlayer
{
	func snapshot(at time: CMTime) -> UIImage?
	{
		let asset = currentItem!.asset
		
		let snapshotGenerator = AVAssetImageGenerator(asset: asset)
		snapshotGenerator.requestedTimeToleranceAfter = CMTime(seconds: 0.0001, preferredTimescale: 1)
		snapshotGenerator.requestedTimeToleranceBefore = CMTime(seconds: 0.0001, preferredTimescale: 1)
		
		do
		{
			let duration = asset.duration
			let position = (time.seconds <= duration.seconds ? time : duration)
			var snapshot = try snapshotGenerator.copyCGImage(at: position, actualTime: nil)
			snapshot = snapshot.copy(colorSpace: CGColorSpace(name: CGColorSpace.sRGB)!)!
			let snapshotImage = UIImage(cgImage: snapshot, scale: UIScreen.main.scale, orientation: .up)
			
			return snapshotImage
		}
		catch
		{
			return nil
		}
	}
}
