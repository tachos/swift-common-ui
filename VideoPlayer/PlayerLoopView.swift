//
//  PlayerLoopView.swift
//
//  Created by Vitaly Trofimov on 13.05.2021.
//  Copyright © 2021 Tachos. All rights reserved.
//

import UIKit
import AVFoundation




class PlayerLoopView: PlayerView
{
	var looper: AVPlayerLooper!
	
	convenience init(url: URL, videoGravity: AVLayerVideoGravity? = nil)
	{
		let player = AVQueuePlayer(url: url)
		self.init(with: player, videoGravity: videoGravity)
		
		looper = AVPlayerLooper(player: player, templateItem: player.currentItem!)
	}
}
