//
//  PlayerView.swift
//
//  Created by Vitaly Trofimov on 13.05.2021.
//  Copyright © 2021 Tachos. All rights reserved.
//

import UIKit
import AVFoundation




class PlayerView: UIView
{
	var playerRateObserver: NSKeyValueObservation!
	var readinessObserver: NSKeyValueObservation!
	var playbackEndObsever: NSObjectProtocol!
	
	private var shouldPlay = false
	private var didPlayToEnd = false
	
	private var snapshotView: UIImageView?
	
	
	convenience init(url: URL, videoGravity: AVLayerVideoGravity? = nil)
	{
		let player = AVPlayer(url: url)
		self.init(with: player, videoGravity: videoGravity)
	}
	
	convenience init(with player: AVPlayer, videoGravity: AVLayerVideoGravity?)
	{
		self.init(frame: .zero)
		
		player.automaticallyWaitsToMinimizeStalling = false
		
		playerLayer.player = player
		
		if let videoGravity = videoGravity
		{
			playerLayer.videoGravity = videoGravity
		}
		
		playerLayer.shouldRasterize = true
		playerLayer.rasterizationScale = UIScreen.main.scale
		
		if !playerLayer.isReadyForDisplay
		{
			addSnapshot()
		}
		
		playerRateObserver = player.observe(\.rate, options: [.new]) { (player, change) in
			//
			let rate = change.newValue!
			if rate == 0 && self.shouldPlay
			{
				if player.currentTime() < player.currentItem!.duration
				{
					player.play()
				}
			}
			else if rate > 0 && !self.shouldPlay
			{
				player.pause()
			}
		}
		
		readinessObserver = playerLayer.observe(\.isReadyForDisplay, options: [.new]) { (_, change) in
			//
			guard !self.didPlayToEnd else
			{
				return
			}
			
			let ready = change.newValue!
			if ready
			{
				self.removeSnapshot()
			}
			else
			{
				if UIApplication.shared.applicationState != .active
				{
					self.addSnapshot()
				}
			}
		}

		playbackEndObsever = NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem!, queue: nil) { (_) in
			//
			if !(player is AVQueuePlayer)
			{
				guard !self.didPlayToEnd else
				{
					return
				}
				
				self.didPlayToEnd = true
				self.shouldPlay = false

				DispatchQueue.main.async {
					self.addSnapshot()
				}
			}
		}
	}
	
	override class var layerClass: AnyClass { AVPlayerLayer.self }
	
	override func didMoveToSuperview()
	{
		if superview == nil
		{
			playerRateObserver = nil
			readinessObserver = nil
			
			if let observer = playbackEndObsever
			{
				NotificationCenter.default.removeObserver(observer)
				playbackEndObsever = nil
			}
		}
	}
}

extension PlayerView
{
	var playerLayer: AVPlayerLayer
	{
		return layer as! AVPlayerLayer
	}
	
	var player: AVPlayer
	{
		return playerLayer.player!
	}
	
	var currentItem: AVPlayerItem?
	{
		return player.currentItem
	}
}

extension PlayerView
{
	func play()
	{
		guard !didPlayToEnd else
		{
			return
		}
		
		shouldPlay = true
		
		player.play()
	}
	
	func pause()
	{
		guard !didPlayToEnd else
		{
			return
		}
		
		shouldPlay = false
		
		player.pause()
	}
}

private extension PlayerView
{
	func addSnapshot()
	{
		if let snapshotView = snapshotView
		{
			snapshotView.removeFromSuperview()
		}
		
		let image = snapshot(at: player.currentTime())
		let view = UIImageView(image: image)
		view.contentMode = (playerLayer.videoGravity == .resizeAspectFill ? .scaleAspectFill : .scaleAspectFit)
		
		addSubview(view)
		view.translatesAutoresizingMaskIntoConstraints = false
		view.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
		view.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
		view.topAnchor.constraint(equalTo: topAnchor).isActive = true
		view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
		
		snapshotView = view
	}
	
	func removeSnapshot()
	{
		if let snapshotView = snapshotView
		{
			snapshotView.removeFromSuperview()
		}
	}
	
	func snapshot(at time: CMTime) -> UIImage?
	{
		return player.snapshot(at: time)
	}
}
