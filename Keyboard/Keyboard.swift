//  Created by Dmitry Cherednikov on 14/11/2018.
//  Copyright © 2018 Averia. All rights reserved.

import IQKeyboardManagerSwift

func setKeyboardDistanceFromTextField(_ distance: CGFloat)
{
    IQKeyboardManager.shared.keyboardDistanceFromTextField = distance
}

func setDefaultKeyboardDistanceFromTextField()
{
    IQKeyboardManager.shared.keyboardDistanceFromTextField = defaultDistanceFromTextField
}

private var defaultDistanceFromTextField: CGFloat
{
    return 10
}
