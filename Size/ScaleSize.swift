//
//  ScaleSize.swift
//  FoodPulse
//
//  Created by Dmitry Cherednikov on 20/03/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit


func scaleSizeProportionaly(_ size: CGSize, targetWidth: CGFloat) -> CGSize
{
    if size.height != 0
    {
        let heightToWidth = size.height / size.width
        let height = heightToWidth * targetWidth

        return CGSize(width: targetWidth, height: height)
    }

    return CGSize(width: targetWidth, height: targetWidth)
}


func snapToScreenScale(_ length: CGFloat) -> CGFloat
{
	let scale = UIScreen.main.scale
	return ceil(length * scale) / scale
}
