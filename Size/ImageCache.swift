//
//  ImageCache.swift
//  FoodPulse
//
//  Created by Dmitry Cherednikov on 20/03/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit
import SDWebImage

private var imageViewCache = [String: UIImageView]()

func getCachedImageSize(for url: URL, imageUpdated: ((CGSize) -> Void)?) -> CGSize
{
    let cache = SDImageCache.shared

    if let image = cache.imageFromCache(forKey: url.absoluteString)
    {
        return image.size
    }
    else
    {
        let imageView = UIImageView()

        imageViewCache[url.absoluteString] = imageView

        imageView.sd_setImage(with: url, completed:
        {
            (image, error, cacheType, _) in

            if error == nil, let image = image
            {
                imageUpdated?(image.size)
            }

            imageViewCache.removeValue(forKey: url.absoluteString)
        })
    }

    return .zero
}
