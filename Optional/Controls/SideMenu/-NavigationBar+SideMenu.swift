//
//  NavigationBar+SideMenu.swift
//  FoodPulse
//
//  Created by Ivan Goremykin on 01.06.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import SideMenu

extension UIViewController
{
    func setupNavigationBarWithMenu(image: UIImage,
                                    rootNavigationController: UINavigationController,
                                    createMenu: () -> UIViewController,
                                    sideMenuWidth: CGFloat = 290,
                                    shadowOpacity: Float = 0.5)
    {
        let menuButton = UIBarButtonItem(image: image,
                                         style: .plain,
                                         target: self,
                                         action: #selector(onMenuButton(_:)))
        self.navigationItem.setLeftBarButton(menuButton, animated: true)

        // UI
        SideMenuManager.default.menuWidth = sideMenuWidth
        SideMenuManager.default.menuShadowOpacity = shadowOpacity
        SideMenuManager.default.menuFadeStatusBar = false
    }

    func createLeftSideMenu() -> UISideMenuNavigationController
    {
        let sideMenuVC = createViewController(.menu, UISideMenuNavigationController.self)
        
        let sideMenuManager = SideMenuManager.default
        sideMenuManager.menuLeftNavigationController = sideMenuVC
        
        let rootNavigationController = self.navigationController ?? rootViewNavigationController!
        sideMenuManager.menuAddPanGestureToPresent(toView: rootNavigationController.navigationBar)
        sideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: rootNavigationController.view, forMenu: .left)

        return sideMenuVC
    }
    
    fileprivate func createAndShowSideMenu(animated: Bool = true, completion: VoidCallback? = nil)
    {
        let sideMenuVC = createLeftSideMenu()

        rootViewController.present(sideMenuVC, animated: animated, completion: completion)
    }
    
    @objc func onMenuButton(_ sender: Any)
    {
        createAndShowSideMenu()
    }
}
