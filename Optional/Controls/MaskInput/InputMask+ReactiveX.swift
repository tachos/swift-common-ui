//
//  InputMask+ReactiveX.swift
//  Averia Collar
//
//  Created by Ivan Goremykin on 14/12/2018.
//  Copyright © 2018 Averia. All rights reserved.
//

import InputMask
import RxSwift
import RxCocoa

public struct MaskedString
{
    let isCompleted: Bool
    let extractedString: String
}

extension Reactive where Base: MaskedTextFieldDelegate
{
    public var delegate: DelegateProxy<MaskedTextFieldDelegate, MaskedTextFieldDelegateListener>
    {
        return RxInputMaskDelegateListenerProxy.proxy(for: base)
    }
    
    public var editingDidBegin: Observable<Void>
    {
        return delegate.sentMessage(#selector(MaskedTextFieldDelegateListener.textFieldDidBeginEditing(_:))).map { value in return () }
    }
    
    public var editingDidChange: Observable<MaskedString>
    {
        return delegate.sentMessage(#selector(MaskedTextFieldDelegateListener.textField(_:didFillMandatoryCharacters:didExtractValue:))).map
        {
            MaskedString(isCompleted: $0[1] as! Bool, extractedString: $0[2] as! String)
        }
    }
    
    public var editingDidEnd: Observable<Void>
    {
        return delegate.sentMessage(#selector(MaskedTextFieldDelegateListener.textFieldDidEndEditing(_:))).map { value in return () }
    }
}

fileprivate class RxInputMaskDelegateListenerProxy: DelegateProxy<MaskedTextFieldDelegate, MaskedTextFieldDelegateListener>, DelegateProxyType, MaskedTextFieldDelegateListener
{
    init(maskedTextFieldDelegate: MaskedTextFieldDelegate)
    {
        super.init(parentObject: maskedTextFieldDelegate, delegateProxy: RxInputMaskDelegateListenerProxy.self)
    }
    
    static func registerKnownImplementations()
    {
        self.register { RxInputMaskDelegateListenerProxy(maskedTextFieldDelegate: $0)}
    }
    
    static func currentDelegate(for object: MaskedTextFieldDelegate) -> MaskedTextFieldDelegateListener?
    {
        return object.listener
    }
    
    static func setCurrentDelegate(_ delegate: MaskedTextFieldDelegateListener?, to object: MaskedTextFieldDelegate)
    {
        object.listener = delegate
    }
}
