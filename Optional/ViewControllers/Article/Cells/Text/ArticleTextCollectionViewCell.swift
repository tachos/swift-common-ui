//  Created by Ivan Goremykin on 24/04/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

class ArticleTextCollectionViewCell: CollectionViewCell<(NSAttributedString, UIEdgeInsets)>
{
    // MARK:- Outlets
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var topInsetConstraint:    NSLayoutConstraint!
    @IBOutlet weak var bottomInsetConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftInsetConstraint:   NSLayoutConstraint!
    @IBOutlet weak var rightInsetConstraint:  NSLayoutConstraint!
    
    // MARK:- Initialize
    override func initialize(_ item: (NSAttributedString, UIEdgeInsets))
    {
        label.attributedText = item.0
        
        topInsetConstraint.constant    = item.1.top
        bottomInsetConstraint.constant = item.1.bottom
        leftInsetConstraint.constant   = item.1.left
        rightInsetConstraint.constant  = item.1.right
    }
}
