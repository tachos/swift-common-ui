//  Created by Ivan Goremykin on 24/04/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import SDWebImage
import UIKit

class ArticleImageCollectionViewCell: CollectionViewCell<ImageResource>
{
    @IBOutlet weak var imageView: UIImageView!
    
    override func initialize(_ imageResource: ImageResource)
    {
        switch imageResource
        {
        case .local(let imageName):
            imageView.image = UIImage(named: imageName)!
            
        case .remote(let imageUrl, let placeholderImageName):
            imageView.sd_setImage(with: imageUrl,
                                  placeholderImage: placeholderImageName == nil ? nil : UIImage(named: placeholderImageName!)!)
        }
    }
}
