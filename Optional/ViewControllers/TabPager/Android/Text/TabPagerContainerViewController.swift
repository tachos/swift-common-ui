//
//  TabContainerViewController.swift
//  Sellel
//
//  Created by Ivan Goremykin on 24.10.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import XLPagerTabStrip

class TabPagerContainerViewController: UIViewController, IndicatorInfoProvider
{
    fileprivate var viewController: UIViewController!
    fileprivate var indicatorInfo: IndicatorInfo!
    
    // MARK:- Constructors
    init(_ viewControllerConstructor: CreateViewControllerCallback, _ title: String)
    {
        super.init(nibName: nil, bundle: nil)
        
        indicatorInfo = IndicatorInfo(title: title)
        viewController = viewControllerConstructor()
    }

    init(childViewController: UIViewController, _ title: String)
    {
        super.init(nibName: nil, bundle: nil)

        indicatorInfo = IndicatorInfo(title: title)
        self.viewController = childViewController
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    // MARK:- XLPagerTabStrip
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo
    {
        return indicatorInfo
    }
}

// MARK:- Lifecycle
extension TabPagerContainerViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        embedViewController(viewController)
    }
}

typealias CreateViewControllerCallback = () -> UIViewController
