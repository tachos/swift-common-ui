//
//  TabPagerPresentationParameters.swift
//  FoodPulse
//
//  Created by Ivan Goremykin on 12.02.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit

struct TabPagerPresentationParameters
{
    let itemTitleColor:             UIColor
    let selectedItemTitleColor:     UIColor
    
    let backgroundColor:            UIColor
    let itemBackgroundColor:        UIColor
    let selectedBarBackgroundColor: UIColor

    let selectedBarHeight: CGFloat
    
    let itemFont: UIFont
    
    let borders:        [UIRectEdge]
    let borderWidth:    CGFloat
    let borderColor:    UIColor
    
    let itemsShouldFillAvailableWidth: Bool
}
