//
//  PagerTabViewController.swift
//  FoodPulse
//
//  Created by Ivan Goremykin on 12.02.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import XLPagerTabStrip

class TabPagerViewController: ButtonBarPagerTabStripViewController
{
    // MARK:- Data
    fileprivate var tabViewControllers: [UIViewController] = []
    fileprivate var tabPagerPresentationParameters: TabPagerPresentationParameters!
    
    fileprivate var navigationBarPresentationMode: NavigationBarPresentationMode?
    
    fileprivate var leftContainerMode: SideContainerMode!
    fileprivate var rightContainerMode: SideContainerMode!

    // Outlets
    @IBOutlet private weak var bottomToSafeAreConstraint: NSLayoutConstraint!
    @IBOutlet private weak var bottomToSuperviewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var rightContainerView: UIView!
    @IBOutlet weak var rightContainerWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var leftContainerView: UIView!
    @IBOutlet weak var leftContainerWidthConstraint: NSLayoutConstraint!
    
    // Initial State
    private var initialTabIndex:           Int? = 0
    private var bottomAttachmentType:      BottomAttachmentType!
    private var onSelectedTabIndexChanged: Handler<Int>?
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController]
    {
        return tabViewControllers
    }

    // workaround from https://github.com/xmartlabs/XLPagerTabStrip/issues/396
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool)
    {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)

        if indexWasChanged && toIndex > -1 && toIndex < viewControllers.count
        {
            onSelectedTabIndexChanged?(toIndex)
        }
    }
}

// MARK: - Lifecycle
extension TabPagerViewController
{
    override func viewDidLoad()
    {
        setupLeftContainer()
        setupRightContainer()
        setup(tabPagerPresentationParameters)
        
        super.viewDidLoad()

        setupBottomConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setupNavigationBar(animated, navigationBarPresentationMode)
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)

        navigateToInitialTabIfRequired()
    }

    private func navigateToInitialTabIfRequired()
    {
        if let initialTabIndex = initialTabIndex
        {
            moveToViewController(at: initialTabIndex, animated: false)
            
            self.initialTabIndex = nil
            
            if initialTabIndex != 0
            {
                deselectFirstCell()
            }
        }
    }
}

// MARK:- Setup
private extension TabPagerViewController
{
    func setup(_ viewControllers:                [UIViewController],
               _ titles:                         [String],
               _ initialTabIndex:                Int,
               _ onSelectedTabIndexChanged:      ((Int) -> Void)?,
               _ tabPagerPresentationParameters: TabPagerPresentationParameters,
               _ navigationBarPresentationMode:  NavigationBarPresentationMode?,
               _ bottomAttachmentType:           BottomAttachmentType,
               _ leftContainerMode:              SideContainerMode = .none,
               _ rightContainerMode:             SideContainerMode = .none)
    {
        self.leftContainerMode = leftContainerMode
        self.rightContainerMode = rightContainerMode
        
        tabViewControllers.reserveCapacity(viewControllers.count)
        for (index, viewController) in viewControllers.enumerated()
        {
            tabViewControllers.append(TabPagerContainerViewController(childViewController: viewController, titles[index]))
        }
        
        self.tabPagerPresentationParameters = tabPagerPresentationParameters
        self.navigationBarPresentationMode = navigationBarPresentationMode

        self.changeCurrentIndex =
        {
            [weak self] oldCell, newCell, animated in

            self?.onChangeCurrentIndex(oldCell, newCell, animated)
        }

        self.onSelectedTabIndexChanged = onSelectedTabIndexChanged

        self.changeCurrentIndexProgressive =
        {
            [weak self] oldCell, newCell, progressPercentage, changeCurrentIndex, animated in

            self?.onChangeCurrentIndexProgressive(oldCell: oldCell,
                                                  newCell: newCell,
                                                  progressPercentage: progressPercentage,
                                                  changeCurrentIndex: changeCurrentIndex,
                                                  animated: animated)
        }
        
        self.initialTabIndex = initialTabIndex
        self.bottomAttachmentType = bottomAttachmentType
    }
    
    func deselectFirstCell()
    {
        let cell = buttonBarView.cellForItem(at: IndexPath(row: 0, section: 0)) as? ButtonBarViewCell
        deselectCell(cell)
    }

    func setupBottomConstraints()
    {
        switch bottomAttachmentType!
        {
        case .superview:
            bottomToSuperviewConstraint.isActive = true

        case .safeArea:
            bottomToSuperviewConstraint.isActive = false
        }
    }
}

// MARK:- UI Setup
private extension TabPagerViewController
{
    func setup(_ parameters: TabPagerPresentationParameters)
    {
        settings.style.buttonBarItemTitleColor      = parameters.itemTitleColor
        settings.style.buttonBarBackgroundColor     = parameters.backgroundColor
        settings.style.selectedBarBackgroundColor   = parameters.selectedBarBackgroundColor
        settings.style.buttonBarItemBackgroundColor = parameters.itemBackgroundColor
        
        settings.style.selectedBarHeight = parameters.selectedBarHeight
        
        settings.style.buttonBarItemFont = parameters.itemFont
        
        settings.style.buttonBarItemsShouldFillAvailableWidth = parameters.itemsShouldFillAvailableWidth
        
        settings.style.buttonBarItemLeftRightMargin = 8

        settings.style.buttonBarMinimumInteritemSpacing = 0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
    }
    
    func setupLeftContainer()
    {
        switch leftContainerMode!
        {
        case .none:
            leftContainerWidthConstraint.constant = 0
            
        case .view(let view, let width):
            leftContainerWidthConstraint.constant = width
            embedView(containerView: leftContainerView, embeddedView: view)
        }
    }
    
    func setupRightContainer()
    {
        switch rightContainerMode!
        {
        case .none:
            rightContainerWidthConstraint.constant = 0
            
        case .view(let view, let width):
            rightContainerWidthConstraint.constant = width
            embedView(containerView: rightContainerView, embeddedView: view)
        }
    }
    
    func setupNavigationBar(_ animated: Bool, _ navigationBarPresentationMode: NavigationBarPresentationMode?)
    {
        if let navigationBarPresentationMode = navigationBarPresentationMode
        {
            switch navigationBarPresentationMode
            {
            case .titleBackRightButton(let screenTitle, let buttonImage, _, let isCustom):
                self.setNavigationBarOpaque1(animated)
                addRightButtonToNavigationBar(buttonImage, #selector(onRightButton), isCustom: isCustom)
                removeTextFromNavigationBarBackButton()
                
                title = screenTitle
                
            case .titleBack:
                self.setNavigationBarOpaque1(animated)
                
            case .hidden:
                self.setNavigationBarHidden1(animated)
            }
        }
    }
    
    private func setNavigationBarOpaque1(_ animated: Bool)
    {
        let navController = navigationController
        navController?.setTransparent1(false)
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setNavigationBarHidden1(_ animated: Bool)
    {
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}

private extension UINavigationController
{
    func setTransparent1(_ transparent: Bool)
    {
        let backgroundImage = transparent ? UIImage() : nil
        let shadowImage = transparent ? UIImage() : UIImage.from(color: .lightGray)
        
        navigationBar.setBackgroundImage(backgroundImage, for: .default)
        navigationBar.shadowImage = shadowImage
    }
}

// MARK:- XLPagerTabStrip
extension TabPagerViewController
{
    func onChangeCurrentIndex(_ oldCell: ButtonBarViewCell?, _ newCell: ButtonBarViewCell?, _ animated: Bool)
    {
        // not working due to some XLPagerTabStrip bug!!!!
        setupTabCellUI(oldCell, newCell)

        onSelectedTabIndexChanged?(currentIndex)
    }
    
    func onChangeCurrentIndexProgressive(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool)
    {
        setupTabCellUI(oldCell, newCell)
    }

    private func setupTabCellUI(_ oldCell: ButtonBarViewCell?, _ newCell: ButtonBarViewCell?)
    {
        deselectCell(oldCell)
        selectCell(newCell)
        
        setupBorders(oldCell)
        setupBorders(newCell)
    }
    
    private func deselectCell(_ cell: ButtonBarViewCell?)
    {
        cell?.label.textColor = tabPagerPresentationParameters.itemTitleColor
    }
    
    private func selectCell(_ cell: ButtonBarViewCell?)
    {
        cell?.label.textColor = tabPagerPresentationParameters.selectedItemTitleColor
    }
    
    private func setupBorders(_ cell: ButtonBarViewCell?)
    {
        if let cell = cell, !tabPagerPresentationParameters.borders.isEmpty
        {
            cell.contentView.addBorders(for: tabPagerPresentationParameters.borders,
                                        width: tabPagerPresentationParameters.borderWidth,
                                        color: tabPagerPresentationParameters.borderColor)
        }
    }
}

// MARK:- UI Handlers
private extension TabPagerViewController
{
    @objc func onRightButton()
    {
        switch navigationBarPresentationMode!
        {
        case .titleBackRightButton(_, _, let buttonCallback, _):
            buttonCallback()
            
        default:
            break
        }
    }
}

// MARK:- Instantiate
struct AndroidTabPagerScreen
{
    static func createTabPagerScreen(titledViewControllers: [(UIViewController, String)],
                                     selectedTabIndex:               Int,
                                     onSelectedTabIndexChanged:      Handler<Int>? = nil,
                                     tabPagerPresentationParameters: TabPagerPresentationParameters,
                                     navigationBarPresentationMode:  NavigationBarPresentationMode?,
                                     bottomAttachmentType:           BottomAttachmentType,
                                     leftContainerMode:              SideContainerMode = .none,
                                     rightContainerMode:             SideContainerMode = .none,
                                     hasTransparentBackground:       Bool = false) -> TabPagerViewController
    {
        let tabPagerViewController = createViewController(storyBoardName:   "TabPager",
                                                          viewControllerId: "TabPagerViewController") as! TabPagerViewController
        
        tabPagerViewController.setup(titledViewControllers.map { $0.0 },
                                     titledViewControllers.map { $0.1 },
                                     selectedTabIndex,
                                     onSelectedTabIndexChanged,
                                     tabPagerPresentationParameters,
                                     navigationBarPresentationMode,
                                     bottomAttachmentType,
                                     leftContainerMode,
                                     rightContainerMode)
        
        if hasTransparentBackground
        {
            tabPagerViewController.view.backgroundColor = .clear
        }
        
        return tabPagerViewController
    }
}

// Taken from: https://stackoverflow.com/a/42654937
extension UIView
{
    func addBorders(for edges:[UIRectEdge], width:CGFloat = 1, color: UIColor = .black)
    {
        if edges.contains(.all)
        {
            layer.borderWidth = width
            layer.borderColor = color.cgColor
        }
        else
        {
            let allSpecificBorders:[UIRectEdge] = [.top, .bottom, .left, .right]
            
            for edge in allSpecificBorders {
                if let v = viewWithTag(Int(edge.rawValue)) {
                    v.removeFromSuperview()
                }
                
                if edges.contains(edge) {
                    let v = UIView()
                    v.tag = Int(edge.rawValue)
                    v.backgroundColor = color
                    v.translatesAutoresizingMaskIntoConstraints = false
                    addSubview(v)
                    
                    var horizontalVisualFormat = "H:"
                    var verticalVisualFormat = "V:"
                    
                    let margin = 0
                    
                    switch edge {
                    case UIRectEdge.bottom:
                        horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                        verticalVisualFormat += "[v(\(width))]-(0)-|"
                    case UIRectEdge.top:
                        horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                        verticalVisualFormat += "|-(0)-[v(\(width))]"
                    case UIRectEdge.left:
                        horizontalVisualFormat += "|-(\(margin))-[v(\(width))]"
                        verticalVisualFormat += "|-(0)-[v]-(0)-|"
                    case UIRectEdge.right:
                        horizontalVisualFormat += "[v(\(width))]-(\(margin))-|"
                        verticalVisualFormat += "|-(0)-[v]-(0)-|"
                    default:
                        break
                    }
                    
                    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: horizontalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: verticalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                }
            }
        }
    }
}

enum BottomAttachmentType
{
    case safeArea
    case superview
}

enum SideContainerMode
{
    case none
    case view(UIView, width: CGFloat)
}

// MARK: ViewControllerContainer
extension TabPagerViewController: ViewControllerContainer
{
    func switchTo(_ selectViewController: @escaping ViewControllerSelector)
    {
        if let viewControllers = containedViewControllers
        {
            var index = 0
            
            while index < viewControllers.count
            {
                if selectViewController(viewControllers[index], index)
                {
                    self.moveToViewController(at: index)
                    break
                }

                index += 1
            }
        }
    }
    
    var containedViewControllers: [UIViewController]?
    {
        return self.viewControllers
    }
}

extension UIViewController
{
    func addRightButtonToNavigationBar(_ image: UIImage, _ action: Selector, isCustom: Bool)
    {
        let barButtonItem = createCustomBarButtonItem(image, action, isCustom: isCustom)
        navigationItem.rightBarButtonItem = barButtonItem
    }

    func removeTextFromNavigationBarBackButton()
    {
        navigationController?.navigationBar.topItem?.title = ""
        navigationItem.title = title
    }
    
    fileprivate func createCustomBarButtonItem(_ image: UIImage, _ action: Selector, isCustom: Bool) -> UIBarButtonItem
    {
        let button = UIButton(type: isCustom ? .custom : .system)
        button.setImage(image, for: .normal)
        button.addTarget(self, action: action, for: .touchUpInside)
        
        return UIBarButtonItem(customView: button)
    }
}


enum NavigationBarPresentationMode
{
    case hidden
    case titleBack(title: String)
    case titleBackRightButton(screenTitle: String, buttonImage: UIImage, buttonCallback: VoidCallback, isCustom: Bool)
}
