import UIKit
import PagingKit

extension Factory
{
    static func createIconTextTabPagerViewController(_ titledViewControllers: [IconTitledViewController],
                                                     _ iconTitledAppearance: IconTitledTabPagerAppearance) -> TwoLineMenuViewController
    {
        let viewController = createViewController(storyBoardName: "TwoLineMenuViewController", viewControllerId: "TwoLineMenuViewController") as! TwoLineMenuViewController
        
        viewController.initialize(titledViewControllers: titledViewControllers, iconTitledAppearance: iconTitledAppearance)
        
        return viewController 
    }
}

class TwoLineMenuViewController: UIViewController
{
    var iconTitledAppearance: IconTitledTabPagerAppearance!
    var titledViewControllers: [IconTitledViewController] = []
    
    func initialize(titledViewControllers: [IconTitledViewController], iconTitledAppearance: IconTitledTabPagerAppearance)
    {
        self.titledViewControllers = titledViewControllers
        self.iconTitledAppearance = iconTitledAppearance
    }
    
    var menuViewController: PagingMenuViewController?
    var contentViewController: PagingContentViewController?
    
    
    lazy var firstLoad: (() -> Void)? = { [weak self, menuViewController, contentViewController] in
        menuViewController?.reloadData()
        contentViewController?.reloadData()
        self?.firstLoad = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuViewController?.register(nib: UINib(nibName: "TwoLineMenuCell", bundle: nil), forCellWithReuseIdentifier: "identifier")
        
        let underlineFocusView = UnderlineFocusView()
        underlineFocusView.underlineHeight = iconTitledAppearance.focusViewHeight
        underlineFocusView.underlineColor = iconTitledAppearance.focusViewBackgroundColor
        
        menuViewController?.registerFocusView(view: underlineFocusView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        firstLoad?()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PagingMenuViewController {
            menuViewController = vc
            menuViewController?.dataSource = self
            menuViewController?.delegate = self
        } else if let vc = segue.destination as? PagingContentViewController {
            contentViewController = vc
            contentViewController?.delegate = self
            contentViewController?.dataSource = self
        }
    }
}

extension TwoLineMenuViewController: PagingMenuViewControllerDataSource {
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "identifier", for: index)  as! TwoLineMenuCell
        
        let titledViewController = titledViewControllers[index]
        
//        contentViewController?.currentPageIndex
//        menuViewController?.visibleCells
        
        cell.initialize(title: titledViewController.title,
                        icon: titledViewController.icon,
                        appearance: iconTitledAppearance)
        
        return cell
    }
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        return viewController.view.bounds.width / CGFloat(titledViewControllers.count)
    }

    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return titledViewControllers.count
    }
}

extension TwoLineMenuViewController: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return titledViewControllers.count
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        return titledViewControllers[index].viewController
    }
}

extension TwoLineMenuViewController: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        contentViewController?.scroll(to: page, animated: true)
    }
}

extension TwoLineMenuViewController: PagingContentViewControllerDelegate {
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        menuViewController?.scroll(index: index, percent: percent, animated: false)
    }
}
