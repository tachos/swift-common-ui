//
//  IconTitledViewController.swift
//  Broadway
//
//  Created by Ivan Goremykin on 17/06/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit

struct IconTitledViewController
{
    let viewController: UIViewController
    
    let icon:  UIImage
    let title: String
}
