//
//  IconTitledTabPagerAppearance.swift
//  Broadway
//
//  Created by Ivan Goremykin on 17/06/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit

struct IconTitledTabPagerAppearance
{
    let normalTextColor: UIColor
    let selectedTextColor:  UIColor
    
    let focusViewHeight: CGFloat
    let focusViewBackgroundColor: UIColor
    
    let font: UIFont
}
