//
//  EnumViewController.swift
//  BBC
//
//  Created by Dmitry Cherednikov on 25/05/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit
import RxSwift

class EnumViewController<EnumType: Equatable>: UIViewController, UITableViewDelegate
{
    // MARK:- Table View
    private(set) var tableView: UITableView!
    private(set) var tableViewStyle = UITableView.Style.plain

    // MARK:- UI Setup
    private let cellIdentifier =    "cellIdentifier"
    private var cellType:           TableViewCell<EnumType>.Type!
    private var presentedElements:  [EnumType]!
    private var handleTap:          ((EnumType) -> Void)!
    private var cellHeightCallback: ((EnumType) -> CGFloat)?
    private var headerView:         UIView?
    private var headerViewHeight:   CGFloat!
    private var footerView:         UIView?
    private var footerViewHeight:   CGFloat!

    // MARK:- Data Source
    private var dataSource: JustDataSource<EnumType>!

    // MARK:- ReactiveX
    private let disposeBag = DisposeBag()

    // MARK:- Lifecycle
    override func viewDidLoad()
    {
        super.viewDidLoad()

        createAndEmbedTableView()
        registerCell()
        setupDataSource()
    }

    // MARK:- UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return cellHeightCallback?(presentedElements[indexPath.row]) ?? Constants.defaultCellHeight
    }

    func update(with presentedElements: [EnumType])
    {
        self.presentedElements = presentedElements

        dataSource.reset(presentedElements)
    }
}

// MARK:- Constants
private extension EnumViewController
{
    struct Constants
    {
        static var defaultCellHeight:       CGFloat { return 44 }
        static var defaultHeaderViewHeight: CGFloat { return 100 }
        static var defaultFooterViewHeight: CGFloat { return 100 }
    }
}

// MARK:- Public Setup
extension EnumViewController
{
    func setup(cellType:           TableViewCell<EnumType>.Type,
               presentedElements:  [EnumType],
               handleTap:          @escaping (EnumType) -> Void,
               cellHeightCallback: ((EnumType) -> CGFloat)? = nil,
               headerView:         UIView? = nil,
               headerViewHeight:   CGFloat = Constants.defaultHeaderViewHeight,
               footerView:         UIView? = nil,
               footerViewHeight:   CGFloat = Constants.defaultFooterViewHeight)
    {
        self.cellType = cellType
        self.presentedElements = presentedElements
        self.handleTap = handleTap
        self.cellHeightCallback = cellHeightCallback
        self.headerView = headerView
        self.headerViewHeight = headerViewHeight
        self.footerView = footerView
        self.footerViewHeight = footerViewHeight
    }
}

// MARK:- Private Setup
private extension EnumViewController
{
    func createAndEmbedTableView()
    {
        tableView = UITableView(frame: view.bounds, style: tableViewStyle)
        tableView.delegate = self

        embedView(containerView: self.view, embeddedView: tableView, intoSafeArea: true)
        
        removeContentOffsetFromTableView()
        addHeaderIfNeeded()
        addFooterIfNeeded()
    }

    private func removeContentOffsetFromTableView()
    {
        if #available(iOS 11, *)
        {
            tableView.contentInsetAdjustmentBehavior = .never
        }
        else
        {
            automaticallyAdjustsScrollViewInsets = false
        }
    }

    private func addHeaderIfNeeded()
    {
        if let headerView = headerView
        {
            let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: headerViewHeight)
            headerView.frame = frame

            tableView.tableHeaderView = headerView
        }
    }

    private func addFooterIfNeeded()
    {
        if let footerView = footerView
        {
            let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: footerViewHeight)
            footerView.frame = frame

            tableView.tableFooterView = footerView
        }
    }

    func registerCell()
    {
        tableView.registerNibCell(cellType)
    }

    func setupDataSource()
    {
        dataSource = JustDataSource(presentedElements)

        bind(dataSource, disposeBag, tableView, cellType)
        handleItemClick(tableView, handleTap, disposeBag, TableViewCell<EnumType>.self) // bind item tapped event
    }
}
