//
//  UIViewController+MailComposeViewController.swift
//  Sellel
//
//  Created by Dmitrii Morozov on 05/12/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import MessageUI
import UIKit

// MARK:- MFMailComposeViewController
extension UIViewController
{
    func createMailComposeViewController(_ mail: Mail) -> MFMailComposeViewController
    {
        let mailComposeViewController = MFMailComposeViewController()
        
        // NOTE: don't forget to call something like:
        //mailComposeViewController.mailComposeDelegate = self
        
        mailComposeViewController.setToRecipients([mail.recipient])
        mailComposeViewController.setSubject(mail.subject)
        mailComposeViewController.setMessageBody(mail.message, isHTML: false)
        
        if let imageData = getData(fromImageUrl: mail.imageUrl)
        {
            mailComposeViewController.addAttachmentData(imageData, mimeType: "image/png", fileName: "Image")
        }
        
        return mailComposeViewController
    }
    
    private func getData(fromImageUrl imageUrl: URL?) -> Data?
    {
        if let savedImageUrl = imageUrl
        {
            if let imageData = NSData(contentsOf: savedImageUrl), let image = UIImage(data: imageData as Data)
            {
                let imageData: Data = image.pngData()!
                
                return imageData
            }
        }
        
        return nil
    }
}

struct Mail
{
    let recipient: String
    let subject:   String
    let message:   String
    
    let imageUrl: URL?
}
