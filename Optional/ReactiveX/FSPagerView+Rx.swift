//
//  FSPagerView+Rx.swift
//  Broadway
//
//  Created by Ivan Goremykin on 23/05/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import FSPagerView
import RxSwift

extension BindData
{
    var toPagerView: BindDataPagerView
    {
        return BindDataPagerView()
    }
}

struct BindDataPagerView
{
    func foo<Item>(_ dataSource: DataSource<[Item]>,
                   _ disposeBag: DisposeBag,
                   
                   _ pagerView: FSPagerView,
                   
                   _ cellProvider: PagerViewCellProvider<Item>)
    {
    }
}

struct PagerViewCellProvider<Item>
{
}
