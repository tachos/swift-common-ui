//  Created by Ivan Goremykin on 18/04/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import SpriteKit

extension SKScene
{
    func convertRect(fromView rect: CGRect) -> CGRect
    {
        let center = convertPoint(fromView: rect.center)
        
        let minPoint = convertPoint(fromView: rect.min)
        let maxPoint = convertPoint(fromView: rect.max)
        
        return CGRect(center: center, size: getSize(minPoint, maxPoint))
    }
    
    func convertRect(toView rect: CGRect) -> CGRect
    {
        let center = convertPoint(toView: rect.center)
        
        let minPoint = convertPoint(toView: rect.min)
        let maxPoint = convertPoint(toView: rect.max)
        
        return CGRect(center: center, size: getSize(minPoint, maxPoint))
    }
    
    func convert(_ rect: CGRect, to node: SKNode) -> CGRect
    {
        let center = convert(rect.center, to: node)
        
        let minPoint = convert(rect.min, to: node)
        let maxPoint = convert(rect.max, to: node)
        
        return CGRect(center: center, size: getSize(minPoint, maxPoint))
    }
}

private func getSize(_ minPoint: CGPoint, _ maxPoint: CGPoint) -> CGSize
{
    return CGSize(width:  abs(maxPoint.x - minPoint.x),
                  height: abs(maxPoint.y - minPoint.y))
}
