//
//  StickToSuperView.swift
//  Broadway
//
//  Created by Ivan Goremykin on 29/05/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import TinyConstraints
import UIKit

extension UIView
{
    /// returns the height constraint (not safe for IOS < 11)
    @discardableResult
    func stickToSuperView(height: CGFloat,
                          usingSafeArea: Bool = true) -> Constraint
    {
        self.topToSuperview(usingSafeArea: usingSafeArea)
        self.leadingToSuperview()
        self.trailingToSuperview()
        return self.height(height)
    }
    
    @discardableResult
    func stickToSuperView(topAnchor: NSLayoutYAxisAnchor,
                         height: CGFloat,
                         usingSafeArea: Bool = true) -> Constraint
    {
        if #available(iOS 11, *)
        {
            self.topToSuperview(usingSafeArea: usingSafeArea)
        }
        else
        {
            self.topToSuperview(topAnchor, offset: 0, isActive: true, usingSafeArea: usingSafeArea)
        }
        
        self.leadingToSuperview()
        self.trailingToSuperview()
        return self.height(height)
    }
}
