//  Created by Ivan Goremykin on 24/04/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

enum ImageResource
{
    case local(imageName: String)
    case remote(url: URL, placeholderImageName: String?)
}

extension ImageResource
{
    init(url: URL?, placeholderImageName: String)
    {
        if let url = url
        {
            self = .remote(url: url, placeholderImageName: placeholderImageName)
        }
        else
        {
            self = .local(imageName: placeholderImageName)
        }
    }
}
