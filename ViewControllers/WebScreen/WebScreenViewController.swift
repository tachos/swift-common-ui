//
//  WebScreenViewController.swift
//  Sellel
//
//  Created by Ivan Goremykin on 26.09.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import WebKit

typealias NavigationBarSetupHandler = (UINavigationController?, Bool) -> Void
typealias RedirectHandler = (URL) -> Bool

class WebScreenViewController: UIViewController
{
    // MARK: Web
    private var url:            URL!
    private var webView:        WKWebView!

    // MARK: UI State
    private var isTransparent:          Bool!
    private var isScrollable:           Bool!
    private var enableNestedNavigation: Bool!

    // MARK: Callbacks
    private var handleRedirect:     RedirectHandler?
    private var setupNavigationBar: NavigationBarSetupHandler?
}

// MARK:- API
extension WebScreenViewController
{
    func updateUrl(_ url: URL)
    {
        if webView.url != url
        {
            webView.load(URLRequest(url: url))

            freezeUI()
        }
    }

    func forceUpdateUrl(_ url: URL)
    {
        webView.load(URLRequest(url: url))

        freezeUI()
    }

    func reload()
    {
        webView.reload()
    }
}

// MARK:- Setup
extension WebScreenViewController
{
    func setup(title:                    String,
               url:                      URL,
               hidesBottomBarWhenPushed: Bool = false,
               isTransparent:            Bool,
               isScrollable:             Bool,
               enableNestedNavigation:   Bool,
               handleRedirect:           RedirectHandler?,
               setupNavigationBar:       NavigationBarSetupHandler?)
    {
        self.title = title
        self.url   = url

        self.hidesBottomBarWhenPushed = hidesBottomBarWhenPushed
        self.isTransparent            = isTransparent
        self.isScrollable             = isScrollable
        self.enableNestedNavigation   = enableNestedNavigation

        self.handleRedirect     = handleRedirect
        self.setupNavigationBar = setupNavigationBar
    }

    private func setupUI()
    {
        if isTransparent
        {
            webView.isOpaque                   = false
            webView.backgroundColor            = .clear
            webView.scrollView.backgroundColor = .clear
        }
        else
        {
            webView.backgroundColor            = .white
        }

        webView.scrollView.isScrollEnabled = isScrollable

        if #available(iOS 11, *)
        {
            webView.scrollView.contentInsetAdjustmentBehavior = .never
        }
        else
        {
            automaticallyAdjustsScrollViewInsets = false
        }
    }
}

// MARK:- Lifecycle
extension WebScreenViewController
{
    override func loadView()
    {
        webView = WKWebView()
        webView.navigationDelegate = self
        webView.uiDelegate = self

        view = webView
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()

        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = false

        freezeUI()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)

        setupNavigationBar?(navigationController, animated)
    }
}

// MARK:- WKNavigationDelegate
extension WebScreenViewController: WKNavigationDelegate
{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    {
        unfreezeUI()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error)
    {
        unfreezeUI(alertTitle: "Error", alertMessage: "Failed to perform an operation")
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void)
    {
        dbgout("[WKWebView]: Redirecting to \(url!)")

        if let url = navigationResponse.response.url,
            let handleRedirect = handleRedirect
        {
            decisionHandler(handleRedirect(url) ? .allow : .cancel)
        }
        else
        {
            decisionHandler(.allow)
        }
    }
}

// MARK:- WKUIDelegate
extension WebScreenViewController: WKUIDelegate
{
    // Opens links with target="_blank"
    // https://stackoverflow.com/questions/25713069/why-is-wkwebview-not-opening-links-with-target-blank
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView?
    {
        if !(navigationAction.targetFrame?.isMainFrame ?? false)
        {
            if enableNestedNavigation,
            let navigationController = self.navigationController,
            let targetUrl = navigationAction.request.url,
            let handleRedirect = handleRedirect, handleRedirect(targetUrl)
            {
                dbgout("[WKWebView]: Opening \(targetUrl)")
                
                pushNewWebViewOntoNavigationStack(url: targetUrl,
                                                  title: self.title ?? self.url.absoluteString,
                                                  navigationController: navigationController)
            }
            else
            {
                dbgout("[WKWebView]: Loading \(navigationAction.request)...")
                
                webView.load(navigationAction.request)
            }
        }

        return nil
    }
    
    private func pushNewWebViewOntoNavigationStack(url: URL,
                                                   title: String,
                                                   navigationController: UINavigationController)
    {
        let newWebScreenViewController = WebScreenViewController()
        
        newWebScreenViewController.setup(title:                    title,
                                         url:                      url,
                                         hidesBottomBarWhenPushed: hidesBottomBarWhenPushed,
                                         isTransparent:            isTransparent,
                                         isScrollable:             isScrollable,
                                         enableNestedNavigation:   enableNestedNavigation,
                                         handleRedirect:           handleRedirect,
                                         setupNavigationBar:       setupNavigationBar)
        
        navigationController.pushViewController(newWebScreenViewController, animated: true)
    }
}

// may be handy for testing & debugging
private
func clearWebCache()
{
    let websiteDataTypes = NSSet(array:
        [
            WKWebsiteDataTypeDiskCache,
            WKWebsiteDataTypeOfflineWebApplicationCache,
            WKWebsiteDataTypeMemoryCache,
            WKWebsiteDataTypeLocalStorage,
            WKWebsiteDataTypeCookies,
            WKWebsiteDataTypeSessionStorage,
            WKWebsiteDataTypeIndexedDBDatabases,
            WKWebsiteDataTypeWebSQLDatabases
        ])
    
    WKWebsiteDataStore.default().removeData(
        ofTypes: websiteDataTypes as! Set<String>,
        modifiedSince: NSDate(timeIntervalSince1970: 0) as Date,
        completionHandler: {}
    )
}
