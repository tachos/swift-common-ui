//  Created by Ivan Goremykin on 15/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import RxSwift
import UIKit

extension UINavigationController
{
    func pushLogScreen()
    {
        pushViewController(createLogViewController(), animated: true)
    }
}

func createLogViewController() -> UIViewController
{
    return LogViewController()
}

class LogViewController: UITableViewController
{
    let disposeBag = DisposeBag()
    
    override func viewDidLoad()
    {
        view.backgroundColor = .white
        title = "Log"
        
        // Data Source
        tableView.registerNibCell(LogEntryTableViewCell.self)
        
        tableView.dataSource = nil
        
        WebServiceLog.instance.observable
            .bind(to: tableView.rx.items(cellIdentifier: String.init(describing: LogEntryTableViewCell.self),
                                                        cellType: LogEntryTableViewCell.self))
            {
                index, error, tableViewCell in
                
                tableViewCell._initialize(error)
            }
            .disposed(by: disposeBag)
        
        // Handle tap
        tableView.delegate = nil
        
        handleItemClick(tableView,
                        navigationController!.pushLogEntryScreen,
                        disposeBag,
                        LogEntryTableViewCell.self)
    }
}
