//  Created by Dmitry Cherednikov on 15/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

class LogEntryTableViewCell: TableViewCell<Error>
{
    @IBOutlet weak var mainLabel: UILabel!
    
    override func initialize(_ error: Error)
    {
        if let error = error as? AlamofireError
        {
            initialize(alamofireError: error)
        }
        else if let error = error as? CustomJsonError
        {
            initialize(customJsonError: error)
        }
        else if let error = error as? ParsingError
        {
            initialize(parsingError: error)
        }
        else
        {
            initialize(unspecifiedError: error)
        }
    }
}

extension LogEntryTableViewCell
{
    

    func initialize(alamofireError: AlamofireError)
    {
        mainLabel.text =
"""
ALAMOFIRE ERROR

\(alamofireError.debugTrace.functionName)
"""
    }

    func initialize(customJsonError: CustomJsonError)
    {
        mainLabel.text =
"""
CUSTOM JSON ERROR

\(customJsonError.debugTrace.functionName)
"""
    }

    func initialize(parsingError: ParsingError)
    {
        mainLabel.text =
"""
PARSING ERROR

\(parsingError.debugTrace.functionName)
"""
    }

    private func initialize(unspecifiedError: Error)
    {
        mainLabel.text = "ERROR"
    }
}
