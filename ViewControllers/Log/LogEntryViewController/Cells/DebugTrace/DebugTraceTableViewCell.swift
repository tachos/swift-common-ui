// Created by Ivan Goremykin on 16/01/2019.
// Copyright © 2019 Tachos. All rights reserved.

import UIKit

class DebugTraceTableViewCell: TableViewCell<String>
{
    // MARK:- Outlets
    @IBOutlet var someTextLabel: UILabel!
    
    // MARK:- TableViewCell
    override func initialize(_ text: String)
    {
        setupUI(text)
    }
}

// MARK:- UI Setup
private extension DebugTraceTableViewCell
{
    func setupUI(_ text: String)
    {
        someTextLabel.text = text
    }
}
