//  Created by Ivan Goremykin on 15/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import RxSwift
import UIKit

extension UINavigationController
{
    func pushLogEntryScreen(error: Error)
    {
        pushViewController(createLogEntryViewController(error: error), animated: true)
    }
}

func createLogEntryViewController(error: Error) -> UIViewController
{
    let viewController = LogEntryViewController()
    
    viewController.initialize(error)
    
    return viewController
}

class LogEntryViewController: UITableViewController
{
    private var error: Error!
    let disposeBag = DisposeBag()
    
    var observable: Observable<[String]>!
    
    func initialize(_ error: Error)
    {
        self.error = error
        
        observable = Observable<[String]>.just(splitToStrings(error))
    }
    
    func splitToStrings(_ error: Error) -> [String]
    {
        switch error
        {
        case is AlamofireError:
            return splitToStrings(error as! AlamofireError)
            
        case is CustomJsonError:
            return splitToStrings(error as! CustomJsonError)
            
        case is ParsingError:
            return splitToStrings(error as! ParsingError)
            
        default:
            return []
        }
    }
    
    private func splitToStrings(_ alamofireError: AlamofireError) -> [String]
    {
        return [
            "\(alamofireError.innerError)",
            "\(alamofireError.debugTrace)",
            "\(alamofireError.requestInfo)"
        ]
    }
    
    private func splitToStrings(_ customJsonError: CustomJsonError) -> [String]
    {
        return [
            "\(customJsonError.innerError)",
            "\(customJsonError.debugTrace)",
            "\(customJsonError.requestInfo)",
            "\(customJsonError.responseInfo)"
        ]
    }
    
    private func splitToStrings(_ parsingError: ParsingError) -> [String]
    {
        return [
            "\(parsingError.requestInfo)",
            "\(parsingError.json)",
            "\(parsingError.parsingTrace)",
            "\(parsingError.debugTrace)",
            "\(parsingError.responseInfo)"
        ]
    }
    
    func getTitle(_ error: Error) -> String
    {
        switch error
        {
        case is AlamofireError:
            return (error as! AlamofireError).debugTrace.functionName
            
        case is CustomJsonError:
            return (error as! CustomJsonError).debugTrace.functionName
            
        case is ParsingError:
            return (error as! ParsingError).debugTrace.functionName
            
        default:
            return "Error Details"
        }
    }
    
    override func viewDidLoad()
    {
        view.backgroundColor = .white
        title = getTitle(error)
        
        // Data Source
        tableView.registerNibCell(DebugTraceTableViewCell.self)

        tableView.dataSource = nil

        observable
            .bind(to: tableView.rx.items(cellIdentifier: String.init(describing: DebugTraceTableViewCell.self),
                                         cellType: DebugTraceTableViewCell.self))
            {
                index, text, tableViewCell in

                tableViewCell._initialize(text)
            }
            .disposed(by: disposeBag)
    }
}
