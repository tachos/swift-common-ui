//
//  UIViewController+InstantiateViewController.swift
//  StarHub
//
//  Created by Ivan Goremykin on 07.06.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

// MARK:- Root

// TODO: move (this has come from BBC)
//func dismissPresentedViewControllers()
//{
//    UIViewController.current.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
//}

func setRootViewController(_ viewController: UIViewController)
{
    let window = UIApplication.shared.keyWindow

    // NOTE: all presented view controllers must be explictly dismissed; otherwise, they'll be left hanging in memory.
    window!.rootViewController?.dismiss(animated: false)

    window!.rootViewController = viewController
}

var rootViewController: UIViewController
{
    return UIApplication.shared.delegate!.window!!.rootViewController!
}

var rootViewNavigationController: UINavigationController?
{
    return rootViewController as? UINavigationController
}

// MARK:- Navigation Controller
var currentTabNavigationController: UINavigationController?
{
    if let rootTabBarController = rootViewController as? UITabBarController
    {
        return rootTabBarController.selectedViewController as? UINavigationController
    }
    else if let firstTabBarViewControllerInStack = rootViewNavigationController?.viewControllers.first as? UITabBarController
    {
        return firstTabBarViewControllerInStack.selectedViewController as? UINavigationController
    }

    return nil
}

// MARK:- Current
extension UIViewController
{
    static var current: UIViewController
    {
        get
        {
            var topController = (UIApplication.shared.keyWindow?.rootViewController)!
            
            while let presentedViewController = topController.presentedViewController
            {
                topController = presentedViewController
            }
            
            return topController
        }
    }
}

// MARK:- Push from NavigationController
extension UIViewController
{
    func pushFromNavigationController(storyBoardName: String, viewControllerId: String)
    {
        let viewController = createViewController(storyBoardName: storyBoardName, viewControllerId: viewControllerId)
        
        pushFromNavigationController(viewController)
    }
    
    func pushFromNavigationController(_ viewController: UIViewController)
    {
        self.navigationController!.pushViewController(viewController, animated: true)
    }
	
	func replaceFromNavigationController(_ viewController: UIViewController)
	{
		self.navigationController!.setViewControllers([viewController], animated: true)
	}
}

// MARK:- Present from Root
extension UIViewController
{
    func presentFromRoot(_ viewController: UIViewController)
    {
        rootViewController.present(viewController, animated: true)
    }
}
