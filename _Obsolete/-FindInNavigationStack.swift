//
//  FindInNavigationStack.swift
//  FoodPulse
//
//  Created by Vyacheslav Shakaev on 12.04.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit

func findViewController<ViewControllerType: UIViewController>(_ navigationController: UINavigationController,
                                                              _ viewControllerType: ViewControllerType.Type) -> ViewControllerType?
{
    return navigationController.viewControllers.first(where: { $0 is ViewControllerType }) as? ViewControllerType
}
