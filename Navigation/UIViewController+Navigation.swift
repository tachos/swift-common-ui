//
//  UIViewController+Navigation.swift
//  Broadway
//
//  Created by Ivan Goremykin on 01/08/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit

extension UIViewController
{
    static var navigation = UIViewControllerNavigation()
    
    class UIViewControllerNavigation
    {
        func setRoot(_ viewController: UIViewController)
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            // NOTE: all presented view controllers must be explictly dismissed; otherwise, they'll be left hanging in memory.
            appDelegate.window!.rootViewController?.dismiss(animated: false)

            appDelegate.window!.rootViewController = viewController
        }
    }
}

var rootViewController: UIViewController
{
    return UIApplication.shared.delegate!.window!!.rootViewController!
}
