//
//  UINavigationController+.swift
//  Broadway
//
//  Created by Ivan Goremykin on 04/10/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit

extension UINavigationController
{
    @discardableResult
    func popToViewController< T: UIViewController >(ofType type: T.Type, animated: Bool) -> T?
    {
        if let viewController = viewControllers.first(where: { $0 is T }) as? T
        {
            popToViewController(viewController, animated: animated)
            return viewController
        }
        
        return nil
    }
}

extension UINavigationController
{
    open override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return topViewController?.preferredStatusBarStyle ?? .default
    }
}

extension UIViewController
{
    func getFirstPreceding<ViewController: UIViewController>(of viewControllerType: ViewController.Type) -> ViewController?
    {
        if let precedingVc = getPrecedingViewController()
        {
            if let ret = precedingVc as? ViewController
            {
                return ret
            }
            else
            {
                return precedingVc.getFirstPreceding(of: ViewController.self)
            }
        }
        
        return nil
    }
    
    private func getPrecedingViewController() -> UIViewController?
    {
        if let navController = self.navigationController,
           let i = navController.viewControllers.firstIndex(of: self)
        {
            return i == 0 ? nil: navigationController?.viewControllers[i-1]
        }
        
        return nil
    }
}

func findViewController<ViewControllerType: UIViewController>(_ navigationController: UINavigationController,
                                                              _ viewControllerType: ViewControllerType.Type) -> ViewControllerType?
{
    return navigationController.viewControllers.first(where: { $0 is ViewControllerType }) as? ViewControllerType
}

// To add completion handlers to push, pop and popToRoot functions
// https://gist.github.com/GoNinja/74ab2cfd0d3c3539d63686e551f6c1b4
//  Created by Rigoberto Sáenz Imbacuán (https://www.linkedin.com/in/rsaenzi/)
//  Copyright © 2017. All rights reserved.
//
extension UINavigationController
{
    func pushViewController(_ viewController: UIViewController, animated: Bool = true, completion: @escaping () -> Void)
    {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }

    func popViewController(animated: Bool = true, completion: @escaping () -> Void)
    {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        popViewController(animated: animated)
        CATransaction.commit()
    }

    func popToRootViewController(animated: Bool = true, completion: @escaping () -> Void)
    {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        popToRootViewController(animated: animated)
        CATransaction.commit()
    }
}
