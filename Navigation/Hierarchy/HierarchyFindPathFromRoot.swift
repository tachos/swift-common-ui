//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

func findSubtree(in instanceTree: Tree<LiveHierarchyNode>,
                 matching expectecNodes: [ExpectedHierarchyNode],
                 found pathFromRoot: inout [Tree<LiveHierarchyNode>],
                 selectors: inout [ViewControllerSelector]) -> Tree<LiveHierarchyNode>?
{
    return findExistingNode_Recursive(in: instanceTree,
                                      matching : expectecNodes,
                                      found: &pathFromRoot,
                                      selectors: &selectors,
                                      0)
}

private
func findExistingNode_Recursive(in instanceTree: Tree<LiveHierarchyNode>,
                                matching expectecNodes: [ExpectedHierarchyNode],
                                found pathFromRoot: inout [Tree<LiveHierarchyNode>],
                                selectors: inout [ViewControllerSelector],
                                _ currentNodeIndex: Int) -> Tree<LiveHierarchyNode>?
{
    if nodesAreEqual(expectecNodes[currentNodeIndex], instanceTree.item)
    {
        //print("✅Matching nodes: \(expectecNodes[currentNodeIndex]) == \(instanceTree.item.debugDescription)")
        
        addViewControllerSelectorIfPresent(expectecNodes[currentNodeIndex], &selectors)
        
        //
        if currentNodeIndex >= expectecNodes.count - 1
        {
            // all constructed nodes match the hierarchy - found the path!
            
            pathFromRoot.append(instanceTree)
            
            var node = instanceTree.parent
            while node != nil
            {
                pathFromRoot.insert(node!, at: 0)
                
                node = node!.parent
            }
            
            return instanceTree
        }
        
        //
        var curr = instanceTree.first_kid
        
        while curr != nil
        {
            let next = curr!.brother
            
            if let foundBranch = findExistingNode_Recursive(in: curr!,
                                                            matching: expectecNodes,
                                                            found: &pathFromRoot,
                                                            selectors: &selectors,
                                                            currentNodeIndex + 1)
            {
                return foundBranch
            }
            
            curr = next
        }
    }
    
    // no match found
    return nil
}

private
func nodesAreEqual(_ typeNode: ExpectedHierarchyNode,
                   _ instanceNode: LiveHierarchyNode) -> Bool
{
    switch (typeNode, instanceNode)
    {
    case (let .navigationNode(navigationNodeA), let .navigationNode(navigationNodeB)):
        
        switch (navigationNodeA, navigationNodeB)
        {
        case (.navigationController, .navigationController):
            return true
            
        case (.presentation, .presentation):
            return true
            
        case (.container(let viewControllerContainerType, _), .container(let viewControllerContainer)):
            return viewControllerContainerType == type(of: viewControllerContainer)
            
        default:
            return false
        }
        
    case (.displayNode(let displayNodeA), .displayNode(let displayNodeB)):
        switch (displayNodeA, displayNodeB)
        {
        case (.viewController(let viewControllerType, _), .viewController(let viewController)):
            return viewControllerType == type(of: viewController)
            
        case (.story(let storyType), .story(let story)):
            return storyType == type(of: story)
            
        default:
            return false
        }
        
    default:
        dbgwarn("Comparing \(typeNode) vs \(instanceNode) -> false")
        return false
    }
}

private
func addViewControllerSelectorIfPresent(_ typeNode: ExpectedHierarchyNode,
                                        _ selectors: inout [ViewControllerSelector])
{
    switch typeNode
    {
    case .navigationNode(let navigationNode):
        switch navigationNode
        {
        case .container(_, let viewControllerSelector):
            selectors.append(viewControllerSelector)
            
        default:
            break
        }
        
    default:
        break
    }
}

///
func getPathFromRootToTopmostScreen(root: Tree<LiveHierarchyNode>,
                                    rootViewController: UIViewController) -> [LiveHierarchyNode]
{
    let topmostViewController = topViewController(for: rootViewController)
//    print("✅topmostViewController: \(topmostViewController)")
    
    //
    var foundNodeDepth = 0
    
    if let node = findHierarchyNode(correspondingTo: topmostViewController,
                                    in: root,
                                    depth: 1,
                                    foundNodeDepth: &foundNodeDepth)
    {
        var pathFromRootToTopmostScreen = [LiveHierarchyNode]()
        pathFromRootToTopmostScreen.reserveCapacity(foundNodeDepth)
        
        //
        pathFromRootToTopmostScreen.append(node.item)
        
        var parent = node.parent
        while parent != nil
        {
            pathFromRootToTopmostScreen.insert(parent!.item, at: 0)
            
            parent = parent!.parent
        }
        
        return pathFromRootToTopmostScreen
    }
    
    return []
}

///
private
func findHierarchyNode(correspondingTo topmostViewController: UIViewController,
                       in tree: Tree<LiveHierarchyNode>,
                       depth: Int,
                       foundNodeDepth: inout Int) -> Tree<LiveHierarchyNode>?
{
    if let associatedVC = getViewController(associatedWith: tree.item),
        associatedVC == topmostViewController
    {
        foundNodeDepth = depth
        return tree
    }
    
    //
    var currentChild = tree.first_kid
    while currentChild != nil
    {
        if let node = findHierarchyNode(correspondingTo: topmostViewController,
                                        in: currentChild!,
                                        depth: depth + 1,
                                        foundNodeDepth: &foundNodeDepth)
        {
            return node
        }
        
        currentChild = currentChild!.brother
    }
    
    return nil
}

///
func getViewController(associatedWith hierarchyNode: LiveHierarchyNode) -> UIViewController?
{
    switch hierarchyNode
    {
    case .navigationNode(let navigationNode):
        switch navigationNode
        {
        case .navigationController:
            return nil
            
        case .presentation(let viewController):
            return viewController
            
        case .container:
            return nil
        }
        
    case .displayNode(let displayNode):
        switch displayNode
        {
        case .viewController(let viewController):
            return viewController
            
        case .story:    // TODO: FIXME - is this correct?
            return nil
        }
    }
}

///
private
func topViewController(for rootViewController: UIViewController) -> UIViewController
{
//dbgout("topViewController: \(rootViewController)")
    if let presentedViewController = rootViewController.presentedViewController
    {
        return topViewController(for: presentedViewController)
    }
    else
    {
        if let navigationController = rootViewController as? UINavigationController,
            let visibleViewController = navigationController.visibleViewController
        {
            return topViewController(for: visibleViewController)
        }
        else if let tabBarController = rootViewController as? UITabBarController,
            let selectedViewController = tabBarController.selectedViewController
        {
            return topViewController(for: selectedViewController)
        }
        else
        {
            if rootViewController.children.count > 0
            {
                dbgwarn("UNIMPL: \(rootViewController) how to determine the topmost VC if an 'ordinary' VC (not a NavCtrl or TabBar) has several children")
                return rootViewController.children.last!
            }
            else
            {
                return rootViewController
            }
        }
    }
}
