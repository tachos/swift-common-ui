//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

//TODO: REFACTOR
protocol RewindBlocker
{
    func canRewind() -> Bool
}
