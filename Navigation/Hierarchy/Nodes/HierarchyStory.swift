//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

enum StoryRewindDecision
{
    case story(Bool)
    case viewControllers
}

// MARK:- Story
class Story
{
    private var currentStoryViewControllerIndex = 0
    private let storyViewControllers: [StoryViewController] // << память не должна здесь утечь
    private weak var navigationController: UINavigationController?
    private let storyRewindDecision: StoryRewindDecision
    
    init(storyViewControllers: [StoryViewController],
         navigationController: UINavigationController,
         storyRewindDecision: StoryRewindDecision)
    {
        self.storyViewControllers = storyViewControllers
        self.navigationController = navigationController
        self.storyRewindDecision = storyRewindDecision
        
        dbgcheck(storyViewControllers.count > 1, "a story must contain at least two view controllers")
        
        //TEMP
        //        for storyViewController in storyViewControllers {
        //            storyViewController.story = Weak(self)
        //        }
    }
    
    /// push the next VC onto the nav stack if the current VC allows it
    func goToNextStepIfPossible() -> Bool
    {
        if storyViewControllers[currentStoryViewControllerIndex].canGoToNextScreen,
            storyViewControllers.isValidIndex(currentStoryViewControllerIndex)
        {
            navigationController?.pushViewController(storyViewControllers[currentStoryViewControllerIndex],
                                                     animated: false)
            
            currentStoryViewControllerIndex += 1
            
            return true
        }
        else
        {
            return false
        }
    }
    
    func goToPreviousStepIfPossible() -> Bool
    {
        if storyViewControllers[currentStoryViewControllerIndex].canGoToNextScreen,
            currentStoryViewControllerIndex > 0 // TODO: can we pop the last remaining VC?
        {
            currentStoryViewControllerIndex -= 1
            
            navigationController?.popViewController(animated: false)
            
            return true
        }
        else
        {
            return false
        }
    }
    
    var viewControllersInNavigationStack: [StoryViewController]
    {
        return Array(storyViewControllers.prefix(currentStoryViewControllerIndex))
    }
}

extension Story: RewindBlocker
{
    func canRewind() -> Bool
    {
        switch storyRewindDecision
        {
        case .story(let allowRewinding):
            return allowRewinding
            
        case .viewControllers:
            //
            dbgcheck(false)
            return false
        }
    }
}

// MARK:- Story ViewController
protocol StoryViewControllerType
{
    //    associatedtype Item
    
    var story: Weak<Story> { get set }
    
    var canGoToNextScreen: Bool { get }
    var canGoToPreviousScreen: Bool { get }
    
    //    var item: Item? { get }
    //    var debugItem: Item { get }
}

typealias StoryViewController = UIViewController & StoryViewControllerType
