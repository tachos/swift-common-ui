//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

// MARK: New
enum NewHierarchyNode
{
    case navigationNode(NewNavigationNode)
    case displayNode(NewDisplayNode)
}

enum NewNavigationNode
{
    case navigationController
    case presentation
    case container(ViewControllerContainer, ViewControllerSelector)
}

enum NewDisplayNode
{
    case viewController(UIViewController)
    case story(StoryDescription)
}

/// for creating new stories
struct StoryDescription
{
    let storyViewControllers: [StoryViewController]
    let storyRewindDecision: StoryRewindDecision
    //let debugname = "Story"
}
