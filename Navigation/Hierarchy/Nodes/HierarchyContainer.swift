//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

// MARK:- Container
protocol ViewControllerContainer
{
    func switchTo(_ selectViewController: @escaping ViewControllerSelector)
    
    var containedViewControllers: [UIViewController]? { get }
}

// MARK: UITabBarController
extension UITabBarController: ViewControllerContainer
{
    func switchTo(_ selectViewController: @escaping ViewControllerSelector)
    {
        if let viewControllers = containedViewControllers
        {
            var index = 0
            
            while index < viewControllers.count
            {
                if selectViewController(viewControllers[index], index)
                {
                    self.selectedIndex = index
                    break
                }
                
                index += 1
            }
        }
    }
    
    var containedViewControllers: [UIViewController]?
    {
        return self.viewControllers
    }
}

// MARK: Selector
typealias ViewControllerSelector = (UIViewController, Int) -> Bool
