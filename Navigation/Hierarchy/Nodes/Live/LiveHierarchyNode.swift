//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

// MARK:- Hierarchy

///
enum LiveHierarchyNode
{
    case navigationNode(LiveNavigationNode)
    case displayNode(LiveDisplayNode)
}

enum LiveNavigationNode
{
    case navigationController(UINavigationController)
    case presentation(UIViewController)
    case container(ViewControllerContainer)
}

enum LiveDisplayNode
{
    case viewController(UIViewController)
    case story(Story)
}

// MARK:- Extensions

extension LiveHierarchyNode
{
    var isPresenting: Bool
    {
        switch self
        {
        case .navigationNode(let navigationNode):
            switch navigationNode
            {
            case .presentation:
                return true
                
            default:
                return false
            }
            
        default:
            return false
        }
    }
    
    var debugDescription: String
    {
        switch self
        {
        case .navigationNode(let navigationNode):
            switch navigationNode
            {
            case .navigationController(let navigationController):
                return "\(navigationController) Pushed on Nav Stack"
                
            case .presentation(let viewController):
                return "\(viewController) Presents"
                
            case .container(let viewControllerContainer):
                return "\(viewControllerContainer) Contains"
            }
            
        case .displayNode(let displayNode):
            switch displayNode
            {
            case .viewController(let viewController):
                return "\(viewController) Displays"
                
            case .story(let story):
                return "\(story) is Story"
            }
        }
    }
}
