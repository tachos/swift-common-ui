//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

// MARK: Existing
enum ExpectedHierarchyNode
{
    case navigationNode(ExpectedNavigationNode)
    case displayNode(ExpectedDisplayNode)
}

enum ExpectedNavigationNode
{
    case navigationController
    case presentation
    case container(ViewControllerContainer.Type, ViewControllerSelector)
    
    init(_ viewControllerContainerType: ViewControllerContainer.Type, _ viewControllerSelector: @escaping ViewControllerSelector)
    {
        self = .container(viewControllerContainerType, viewControllerSelector)
    }
}

enum ExpectedDisplayNode
{
    case viewController(UIViewController.Type, setupCallback: ViewControllerCallback?)
    case story(Story.Type)
}

func expectedNodesContainNavigationControllerNode(_ expectecNodes: [ExpectedHierarchyNode]) -> Bool
{
    return expectecNodes.contains(where: { expectedHierarchyNode -> Bool in
        switch expectedHierarchyNode
        {
        case .navigationNode(let existingNavigationNode):
            switch existingNavigationNode
            {
            case .navigationController:
                return true
                
            default:
                return false
            }
            
        default:
            return false
        }
    })
}
