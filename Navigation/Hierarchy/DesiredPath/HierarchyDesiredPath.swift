//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

// MARK:- Desired
struct DesiredPath
{
    let expectedNodes: [ExpectedHierarchyNode]
    let newNodes: [NewHierarchyNode]
    
    init(_ expectedNodes: [ExpectedHierarchyNode], _ newNodes: [NewHierarchyNode])
    {
        self.expectedNodes = expectedNodes
        self.newNodes = newNodes
    }
}
