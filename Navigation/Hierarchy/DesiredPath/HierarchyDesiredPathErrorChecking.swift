//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

func checkForErrors(desiredPath: DesiredPath) -> OpenPathResult
{
    var foundNavigationController = false
    var foundStoryDescription: StoryDescription?
    
    //
    for newNode in desiredPath.newNodes
    {
        switch newNode
        {
        case .navigationNode(let navigationNode):
            switch navigationNode
            {
            case .navigationController:
                foundNavigationController = true
                
            case .container:
                break
                
            case .presentation:
                break
            }
            
        case .displayNode(let newDisplayNode):
            switch newDisplayNode
            {
            case .viewController:
                if foundStoryDescription != nil
                {
                    return .failure(Either2(.cannotShowNewNodesAfterStory(story: foundStoryDescription!, newNode: newNode)))
                }
                
            case .story(let storyDescription):
                if !foundNavigationController
                {
                    if !expectedNodesContainNavigationControllerNode(desiredPath.expectedNodes)
                    {
                        return .failure(Either2(.noNavigationControllerFoundToShowStory(desiredPath: desiredPath)))
                    }
                }
                
                foundStoryDescription = storyDescription
            }
        }
    }
    
    return .success
}
