//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

///
enum OpenPathResult
{
    case success
    case failure(HierarchyErrorType)
}

typealias HierarchyErrorType = Either2<HierarchyDescriptionError, HierarchyRuntimeError>

extension OpenPathResult
{
    var isOk: Bool
    {
        switch self
        {
        case .success:
            return true
            
        default:
            return false
        }
    }
}
