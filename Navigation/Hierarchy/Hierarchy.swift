//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

// MARK:- Config


// MARK:- API

//private
func getHierarchy() -> Tree<LiveHierarchyNode>
{
    let keyWindow = UIApplication.shared.keyWindow
    let rootVC = keyWindow!.rootViewController
    
    return convertToLiveHierarchyNode_recursive(rootVC!)
}

///
func getHierarchy(rootViewController: UIViewController) -> Tree<LiveHierarchyNode>
{
    return convertToLiveHierarchyNode_recursive(rootViewController)
}

//-------------------------------------------------------------------------------------------//

///
func openPathIfPossible(hierarchy: Tree<LiveHierarchyNode>,
                        rootViewController: UIViewController,
                        _ desiredPath: DesiredPath) -> OpenPathResult
{
    let resultOfCheckingDesiredPath = checkForErrors(desiredPath: desiredPath)
    if !resultOfCheckingDesiredPath.isOk
    {
        return resultOfCheckingDesiredPath
    }
    
    //
    let typeHierarchy = convertInstanceTreeToTypeTree(hierarchy)
    let _ = typeHierarchy
//    dbgout("Printing out the 'dead'/type hierarchy...")
//    typeHierarchy.dbgprint()

    //
    var pathFromRoot = [Tree<LiveHierarchyNode>]()
    pathFromRoot.reserveCapacity(desiredPath.expectedNodes.count)

    var foundSelectors = [ViewControllerSelector]()
    foundSelectors.reserveCapacity(desiredPath.expectedNodes.count)

    
    
    //
    guard let deepestNodeThatWontBeClosed = findSubtree(in: hierarchy,
                                                        matching: desiredPath.expectedNodes,
                                                        found: &pathFromRoot,
                                                        selectors: &foundSelectors)
    else
    {
        return .failure(Either2(.desiredPathNotFound(hierarchy, desiredPath.expectedNodes)))
    }
    
//    dbgout("✅Deepest node that won't be closed: \(deepestNodeThatWontBeClosed.item.debugDescription)")
    
    
    
    
    
#if false
    //
    let pathFromRootToTopmostScreen = getPathFromRootToTopmostScreen(root: hierarchy,
                                                                     rootViewController: rootViewController)
    if pathFromRootToTopmostScreen.count == 0
    {
        return .failure(Either2(.cannotFindPathFromRootToTopmostViewController(liveHierarchy: hierarchy,
                                                                               rootViewController: rootViewController)))
    }
    
    //
//    print("✅pathFromRootToTopmostScreen:")
//    print(pathFromRootToTopmostScreen)
    
    //
    if let unwindBlocker = findUnwindBlocker(pathFromRootToTopmostScreen: pathFromRootToTopmostScreen)
    {
        return .failure(Either2(.rejectedUnwinding(pathFromRootToTopmostScreen: pathFromRootToTopmostScreen, unwindBlocker)))
    }
#endif

    
    
    
    
    
    //
    if !unwindChildren(of: deepestNodeThatWontBeClosed)
    {
        return .failure(Either2(.couldNotCloseExistingScreens(deepestNodeThatWontBeClosed: deepestNodeThatWontBeClosed)))
    }
    
    //
    deepestNodeThatWontBeClosed.removeAllChildren()
    
    //
//hierarchy.dbgprint()
    
    //
    setActiveTabInViewContainers(in: pathFromRoot,
                                 using: foundSelectors)

    callSetupCallbacks(for: pathFromRoot,
                       using: desiredPath.expectedNodes)
    
    //
    let resultOfOpeningNewScreens = openNewScreens(startingFrom: deepestNodeThatWontBeClosed,
                                                   using: desiredPath.newNodes)
    if !resultOfOpeningNewScreens.isOk
    {
        return resultOfOpeningNewScreens
    }

    return .success
}

///
private
func findUnwindBlocker(pathFromRootToTopmostScreen: [LiveHierarchyNode]) -> RewindBlocker?
{
    for hierarchyNode in pathFromRootToTopmostScreen
    {
        if let vc = getViewController(associatedWith: hierarchyNode),
        let rewindBlocker = vc as? RewindBlocker
        {
            if !rewindBlocker.canRewind()
            {
                return rewindBlocker
            }
        }
    }
    
    return nil
}

//--------------------------------------------------------------------------------------------//

private
func unwindChildren(of instanceTree: Tree<LiveHierarchyNode>) -> Bool
{
    return unwindChildren_Recursive(of: instanceTree)
}

private
func unwindChildren_Recursive(of instanceTree: Tree<LiveHierarchyNode>) -> Bool
{
    var curr = instanceTree.first_kid
    
    while curr != nil
    {
        let next = curr!.brother
        
        if !unwindChildren_Recursive(of: curr!)
        {
            return false
        }
        
        curr = next
    }
    
    // all children unwound/closed - try to close the current node
    
//    dbgout("Closing item \(instanceTree.item.debugDescription)...")
    
    switch instanceTree.item
    {
    case .navigationNode(let navigationNode):
        switch navigationNode
        {
        case .navigationController(let navigationController):
            navigationController.popToRootViewController(animated: false)
            return true
            
        case .presentation(let viewController):
            viewController.dismiss(animated: false,
                                   completion:
            {
                //
            })
            return true

        case .container:
            return true
        }

    case .displayNode:
        return true
    }
}

private
func getDisplayedViewController(_ liveNode: LiveHierarchyNode) -> UIViewController?
{
    switch liveNode
    {
    case .navigationNode:
        return nil
        
    case .displayNode(.viewController(let viewController)):
        return viewController
        
    default:
        return nil
    }
}

private
func callSetupCallbacks(for pathFromRoot: [Tree<LiveHierarchyNode>],
                        using expectecNodes: [ExpectedHierarchyNode])
{
    let nodeCount = pathFromRoot.count
    
    dbgcheck(nodeCount == expectecNodes.count)
    
    //
    var nodeIndex = 0

    //
    while nodeIndex < nodeCount
    {
        let liveInstance = pathFromRoot[nodeIndex].item
        let templateNode = expectecNodes[nodeIndex]

        switch templateNode
        {
        case .displayNode(let displayNode):
            switch displayNode
            {
            case .viewController(let viewControllerType, let setupCallback):
                let vc = getDisplayedViewController(liveInstance)
                dbgcheck(type(of: vc!) == viewControllerType)
                if let setupCallback = setupCallback
                {
                    setupCallback(vc!)
                }
                break
                
            case .story://TODO: FIXME LATER
                break
            }
            
        default:
            break
        }
        
        nodeIndex += 1
    }
}

private
func setActiveTabInViewContainers(in pathFromRoot: [Tree<LiveHierarchyNode>],
                                  using selectors: [ViewControllerSelector])
{
    let nodeCount = pathFromRoot.count
    
    dbgcheck(selectors.count <= nodeCount)
    
    //
    var nodeIndex = 0
    var viewControllerSelectorIndex = 0
    
    //
    while nodeIndex < nodeCount
    {
        let liveInstance = pathFromRoot[nodeIndex].item

        switch liveInstance
        {
        case .navigationNode(let navigationNode):
            switch navigationNode
            {
            case .container(let viewControllerContainer):
                let viewControllerSelector = selectors[viewControllerSelectorIndex]
                viewControllerSelectorIndex += 1
                //TODO: error handling?
                viewControllerContainer.switchTo(viewControllerSelector)
                break
                
            default:
                break
            }

        default:
            break
        }

        nodeIndex += 1
    }
}

private
func findNearestNavigationNode(in treeNode: Tree<LiveHierarchyNode>) -> Tree<LiveHierarchyNode>?
{
    switch treeNode.item
    {
    case .navigationNode(let liveNavigationNode):
        switch liveNavigationNode
        {
        case .navigationController, .presentation:
            return treeNode

        case .container:
            // We cannot push or present from TabBar/TabPager.
            //TODO: AAA - check if this is correct!
            if let parent = treeNode.parent
            {
                return findNearestNavigationNode(in: parent)
            }
        }
        
    case .displayNode:
        if let parent = treeNode.parent
        {
            return findNearestNavigationNode(in: parent)
        }
    }
    return nil
}

private
func getDisplayedViewController(_ newNode: NewHierarchyNode) -> UIViewController?
{
    switch newNode
    {
    case .navigationNode:
        return nil
        
    case .displayNode(.viewController(let viewController)):
        return viewController
        
    default:
        return nil
    }
}

private
func findNearestNavigationController(in treeNode: Tree<LiveHierarchyNode>) -> UINavigationController?
{
    switch treeNode.item
    {
    case .navigationNode(let navigationNode):
        switch navigationNode
        {
        case .navigationController(let navigationController):
            return navigationController
            
        case .container:
            break
            
        case .presentation:
            break
        }

    case .displayNode:
        break
    }
    
    if let parent = treeNode.parent
    {
        return findNearestNavigationController(in: parent)
    }
    
    return nil
}

private
func findExistingViewControllerToPresentFrom(in treeNode: Tree<LiveHierarchyNode>) -> UIViewController?
{
    switch treeNode.item
    {
    case .navigationNode:
        if let parent = treeNode.parent
        {
            return findExistingViewControllerToPresentFrom(in: parent)
        }
        
    case .displayNode(let displayNode):
        switch displayNode
        {
        case .viewController(let viewController):
            return viewController
            
        case .story(_):
            debugUnreachable()
        }
    }
    return nil
}

private
func presentNewViewControllerIfPossible(newNodes: [NewHierarchyNode],
                                        indexOfPresentingNewNode: Int,
                                        existingTree: Tree<LiveHierarchyNode>) -> OpenPathResult
{
    let expectedIndexOfNewViewControllerToPresent = indexOfPresentingNewNode + 1
    
    if expectedIndexOfNewViewControllerToPresent >= newNodes.count
    {
        return .failure(Either2(.reachedEndOfNewNodesNotFoundViewControllerToPresent(newNodes: newNodes,
                                                                                     indexOfPresentingNewNode: indexOfPresentingNewNode)))
    }
    
    //
    let expectedNewNodeToPresent = newNodes[expectedIndexOfNewViewControllerToPresent]
    
    if let newViewControllerToPresent = getDisplayedViewController(expectedNewNodeToPresent)
    {
        if let existingViewControllerToPresentFrom = findExistingViewControllerToPresentFrom(in: existingTree)
        {
            existingViewControllerToPresentFrom.present(newViewControllerToPresent,
                                                        animated: false,
                                                        completion: {})
            return .success
        }
        else
        {
            return .failure(Either2(.cannotFindViewControllerToPresentFrom(newNodeToPresent: expectedNewNodeToPresent,
                                                                           existingTree: existingTree)))
        }
    }
    else
    {
        return .failure(Either2(.noViewControllerInNewNodeThatCanBePresented(expectedNewViewControllerToPresent: expectedNewNodeToPresent,
                                                                             existingTree: existingTree)))
    }
}

private
func pushOrPresentNewViewController(newViewController: UIViewController,
                                    existingTree: Tree<LiveHierarchyNode>) -> OpenPathResult
{
    let nearestNavigationNode = findNearestNavigationNode(in: existingTree)
    if nil == nearestNavigationNode
    {
        dbgwarn("Couldn't find nearest navigation node!")
        return .failure(Either2(.couldntFindExistingNavigationNodeToShowViewController(liveNode: existingTree,
                                                                                       newViewController: newViewController)))
    }

    //
    switch nearestNavigationNode!.item
    {
    case .navigationNode(let navigationNode):
        switch navigationNode
        {
        case .navigationController(let navigationController):
            navigationController.pushViewController(newViewController, animated: false)
            
        case .presentation:
            debugUnreachable("unimpl")
            
        case .container(let viewControllerContainer):
            debugUnreachable("Dunno how to display over container: \(viewControllerContainer)")
        }
        
    case .displayNode(let displayNode):
        switch displayNode
        {
        case .viewController(let viewController):
            viewController.present(newViewController,
                                   animated: false,
                                   completion: {})

        case .story(_):
            debugUnreachable()
        }
    }

    return .success
}

//--------------------------------------------------------------------------------------------//

private
func openNewScreens(startingFrom existingTree: Tree<LiveHierarchyNode>,
                    using newNodes: [NewHierarchyNode]) -> OpenPathResult
{
    var newNodeIndex = 0
    while newNodeIndex < newNodes.count
    {
        let newNode = newNodes[newNodeIndex]

        switch newNode
        {
        case .navigationNode(let newNavigationNode):
            switch newNavigationNode
            {
            case .navigationController:
                debugUnreachable()
                
            case .container:
                debugUnreachable()
                
            case .presentation:
                let result = presentNewViewControllerIfPossible(newNodes: newNodes,
                                                                indexOfPresentingNewNode: newNodeIndex,
                                                                existingTree: existingTree)
                
                if !result.isOk
                {
                    return result
                }

                newNodeIndex += 1
            }
            
        case .displayNode(let newDisplayNode):
            switch newDisplayNode
            {
            case .viewController(let newViewController):
                let result = pushOrPresentNewViewController(newViewController: newViewController,
                                                            existingTree: existingTree)
                if !result.isOk
                {
                    return result
                }
                
            case .story(let storyDescription):
                let nearestNavigationController = findNearestNavigationController(in: existingTree)
                if nil == nearestNavigationController
                {
                    dbgwarn("Couldn't find nearest navigation node to push a new Story!")
                    return .failure(Either2(.couldntFindExistingNavigationNodeToShowStory(liveNode: existingTree,
                                                                                          storyDescription: storyDescription)))
                }

                let story = Story(storyViewControllers: storyDescription.storyViewControllers,
                                   navigationController: nearestNavigationController!,
                                   storyRewindDecision: storyDescription.storyRewindDecision)
    
                // NOTE: assume that there's no view controllers after this Story
                return story.goToNextStepIfPossible() ? .success : .failure(Either2(.storyRejectedGoingToNextStep))
            }
        }

        newNodeIndex += 1
    }

    return .success
}

func convertInstanceTreeToTypeTree(_ instanceTree: Tree<LiveHierarchyNode>) -> Tree<ExpectedHierarchyNode>
{
    func dummyUnusedViewControllerSelector(vc: UIViewController, index: Int) -> Bool
    {
        return false
    }
    
    return instanceTree.map
    {
        hierarchyNode -> ExpectedHierarchyNode in

        switch hierarchyNode
        {
        case .navigationNode(let navigationNode):
            switch navigationNode
            {
            case .navigationController:
                return .navigationNode( ExpectedNavigationNode.navigationController )
                
            case .presentation:
                return .navigationNode( ExpectedNavigationNode.presentation )
                
            case .container(let viewControllerContainer):
                return .navigationNode( ExpectedNavigationNode.container(type(of: viewControllerContainer), dummyUnusedViewControllerSelector) )
            }
            
        case .displayNode(let displayNode):
            switch displayNode
            {
            case .viewController(let viewController):
                return .displayNode(.viewController(type(of: viewController), setupCallback: nil))
                
            case .story(let story):
                return .displayNode(.story(type(of: story)))
            }
        }
    }
}

//--------------------------------------------------------------------------------------------//

private
func convertToLiveHierarchyNode_recursive(_ viewController: UIViewController) -> Tree<LiveHierarchyNode>
{
    if let presentedViewController = viewController.presentedViewController,
    !(presentedViewController === viewController.parent?.presentedViewController)
    {
        //print("✅\(presentedViewController) is presented from \(viewController)")

        let presentedTreeNode = convertToLiveHierarchyNode_recursive(presentedViewController)

        let newHierarchyNode = LiveHierarchyNode.navigationNode(.presentation(viewController))

        let newTreeNode = Tree<LiveHierarchyNode>(item: newHierarchyNode)
        
        for childVC in viewController.children
        {
            newTreeNode.addChild(convertToLiveHierarchyNode_recursive(childVC))
        }
        
        newTreeNode.addChild(presentedTreeNode)
        
        return newTreeNode
    }
    else
    {
        switch viewController
        {
        case is UINavigationController:
            let navigationController = viewController as! UINavigationController

            //
            let newNavigationTreeNode = Tree<LiveHierarchyNode>(item: .navigationNode(.navigationController(navigationController)))

            //
            var previousStory: Story?
            var previousStoryTreeNode: Tree<LiveHierarchyNode>?
            
            for childViewController in navigationController.viewControllers
            {
                let childTreeNode = convertToLiveHierarchyNode_recursive(childViewController)
                
                if let storyViewController = childViewController as? StoryViewController
                {
//                    dbgout("storyViewController: \(type(of: storyViewController))")
                    let currentStory = storyViewController.story.value!
                    
                    if nil == previousStoryTreeNode
                    {
                        previousStoryTreeNode = Tree<LiveHierarchyNode>(item: .displayNode(.story(currentStory)))
                        previousStory = currentStory
                    }

                    previousStoryTreeNode!.addChild(childTreeNode)
                    
                    if !(previousStory === currentStory)
                    {
                        newNavigationTreeNode.addChild(previousStoryTreeNode!)

                        previousStoryTreeNode = Tree<LiveHierarchyNode>(item: .displayNode(.story(currentStory)))
                        previousStory = currentStory
                    }
                }
                else
                {
                    if nil != previousStory
                    {
                        dbgwarn("NavCtrl must contain only story view controllers!")
                    }
                    
                    newNavigationTreeNode.addChild(childTreeNode)
                }
            }//for
            
            if let remainingStoryTreeNode = previousStoryTreeNode
            {
                newNavigationTreeNode.addChild(remainingStoryTreeNode)
            }

//newNavigationTreeNode.dbgprint()
            
            return newNavigationTreeNode

        case is ViewControllerContainer:
            let viewControllerContainer = viewController as! ViewControllerContainer
            
            let newHierarchyNode = LiveHierarchyNode.navigationNode(.container(viewControllerContainer))

            let newTreeNode = Tree<LiveHierarchyNode>(item: newHierarchyNode)

            //
            if let containedViewControllers = viewControllerContainer.containedViewControllers
            {
                for childVC in containedViewControllers
                {
                    newTreeNode.addChild(convertToLiveHierarchyNode_recursive(childVC))
                }
            }

            return newTreeNode
            
        default:
            let newHierarchyNode = LiveHierarchyNode.displayNode(.viewController(viewController))
            
            let newTreeNode = Tree<LiveHierarchyNode>(item: newHierarchyNode)
            
            for childVC in viewController.children
            {
                newTreeNode.addChild(convertToLiveHierarchyNode_recursive(childVC))
            }
            
            return newTreeNode
        }
    }
}

//--------------------------------------------------------------------------------------------//

#if false // FoodPulse

func test1_openPathIfPossible()
{
    let tree = getHierarchy()
    tree.dbgprint()
    
    let typeTree = convertInstanceTreeToTypeTree(tree)
    typeTree.dbgprint()
    
    //
    let expectedNodes: [ExpectedHierarchyNode] =
        [
            //            .navigationNode(.container(MainViewController.self,
            //                                       {
            //                                        viewController, index in
            //                                        return index == 0   // select the first tab (MyPulseViewController)
            //            })),
            .navigationNode(.navigationController),
            //            .displayNode(.viewController(MyPulseViewController.self)),
            
            //            .displayNode(.viewController(CompareTabViewController.self)),
            //            .displayNode(.viewController(UnsyncedRestaurantContainerViewController.self)),
            //            .displayNode(.viewController(CompareViewController.self)),
            //            .displayNode(.viewController(TabPagerViewController.self)),
            //            .displayNode(.viewController(TabPagerContainerViewController.self)),
            //            .displayNode(.viewController(UIViewController.self)),
            //            .displayNode(.viewController(RestaurantsViewController.self)),
    ]
    
    //
    // crashes when pushed
    //    let newVC = createRestaurant(restaurantId: "0x3CDF9A092584F8C7",    // "Amos Cafe Singapore"
    //                                    restaurantPresentationMode: .peopleSayingTags)
    
    let newVC = UIViewController()
    newVC.view.backgroundColor = .blue
    
    let newNodes: [NewHierarchyNode] =
        [
            .displayNode(.viewController(newVC)),
            ]
    
    //
    let hierarchy = getHierarchy()
    
    let result = openPathIfPossible(hierarchy: hierarchy,
                                    DesiredPath(expectedNodes, newNodes))
//    print(result)
}

func testHierarchy_present()
{
    let hierarchy = getHierarchy()
    hierarchy.dbgprint()
    
    let typeTree = convertInstanceTreeToTypeTree(hierarchy)
    typeTree.dbgprint()
    
    let expectedNodes: [ExpectedHierarchyNode] =
        [
            .navigationNode(.container(MainViewController.self,
                                       {
                                        viewController, index in
                                        return index == 0   // select the first tab (MyPulseViewController)
            })),
            
            .navigationNode(.navigationController),
            
            .displayNode(.viewController(MyPulseViewController.self, setupCallback: nil)),
            
            .displayNode(.viewController(UnsyncedRestaurantContainerViewController.self, setupCallback: nil)),
            .displayNode(.viewController(WebRestaurantViewController.self, setupCallback: nil)),
            .displayNode(.viewController(WebScreenViewController.self, setupCallback: nil)),
            ]
    
    //
    let newVC = UIViewController()
    newVC.view.backgroundColor = .blue
    
    let newNodes: [NewHierarchyNode] =
        [
            .navigationNode(.presentation),
            .displayNode(.viewController(newVC)),
            ]
    
    //
    let result = openPathIfPossible(hierarchy: hierarchy,
                                    DesiredPath(expectedNodes, newNodes))
//    print(result)
}

#endif
