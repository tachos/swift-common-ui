//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

///
extension Tree where Item == LiveHierarchyNode
{
    func dbgprint()
    {
        dbgprint_recursive(depth: 0)
    }
    
    private func dbgprint_recursive(depth: Int)
    {
        let childCount = countChildren()
        
        let parentIsPresenting = parent?.item.isPresenting ?? false
        let prefix = (parentIsPresenting && (self === parent?.first_kid)) ? "P " : ""
        let _ = prefix
        
        let blanks = String(repeating: "    ", count: depth)
//        print("\(blanks)\(prefix)\(item.debugDescription) (\(childCount) children)")
        let _ = blanks

        if childCount != 0
        {
//            print("\(blanks)    |")
            
            //
            var curr = first_kid
            
            while curr != nil
            {
                let next = curr!.brother
                
                curr!.dbgprint_recursive(depth: depth+1)
                
                curr = next
            }
        }
    }
}

///
extension Tree where Item == ExpectedHierarchyNode
{
    func dbgprint()
    {
        dbgprint_recursive(depth: 0)
    }
    
    private func dbgprint_recursive(depth: Int)
    {
        let childCount = countChildren()
        
        let blanks = String(repeating: "    ", count: depth)
//        print("\(blanks)\(item) (\(childCount) children)")
        let _ = blanks

        if childCount != 0
        {
//            print("\(blanks)    |")
            
            //
            var curr = first_kid
            
            while curr != nil
            {
                let next = curr!.brother
                
                curr!.dbgprint_recursive(depth: depth+1)
                
                curr = next
            }
        }
    }
}
