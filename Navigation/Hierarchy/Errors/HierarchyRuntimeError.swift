//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

///
enum HierarchyRuntimeError: Error
{
    case desiredPathNotFound(Tree<LiveHierarchyNode>, [ExpectedHierarchyNode])
    
    case rejectedUnwinding(pathFromRootToTopmostScreen: [LiveHierarchyNode], RewindBlocker)
    
    case couldNotCloseExistingScreens(deepestNodeThatWontBeClosed: Tree<LiveHierarchyNode>)
    
    case couldntFindExistingNavigationNodeToShowViewController(liveNode: Tree<LiveHierarchyNode>,
        newViewController: UIViewController)
    
    case couldntFindExistingNavigationNodeToShowStory(liveNode: Tree<LiveHierarchyNode>,
        storyDescription: StoryDescription)
    
    case cannotFindViewControllerToPresentFrom(newNodeToPresent: NewHierarchyNode,
        existingTree: Tree<LiveHierarchyNode>)
    
    case cannotFindPathFromRootToTopmostViewController(liveHierarchy: Tree<LiveHierarchyNode>,
        rootViewController: UIViewController)
    
    case storyRejectedGoingToNextStep
}

extension HierarchyRuntimeError: Equatable
{
    static func ==(lhs: HierarchyRuntimeError, rhs: HierarchyRuntimeError) -> Bool
    {
        switch (lhs, rhs)
        {
        case (.desiredPathNotFound, .desiredPathNotFound):
            return true
            
        case (.rejectedUnwinding, .rejectedUnwinding):
            return true
            
        case (.couldNotCloseExistingScreens, .couldNotCloseExistingScreens):
            return true
            
        case (.couldntFindExistingNavigationNodeToShowViewController, .couldntFindExistingNavigationNodeToShowViewController):
            return true
            
        case (.cannotFindViewControllerToPresentFrom, .cannotFindViewControllerToPresentFrom):
            return true
            
        case (.cannotFindPathFromRootToTopmostViewController, .cannotFindPathFromRootToTopmostViewController):
            return true
            
        default:
            return false
        }
    }
}
