//  Created by Ivan Goremykin on 18/01/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

///
enum HierarchyDescriptionError: Error
{
    case noNavigationControllerFoundToShowStory(desiredPath: DesiredPath)
    
    case reachedEndOfNewNodesNotFoundViewControllerToPresent(newNodes: [NewHierarchyNode],
        indexOfPresentingNewNode: Int)
    
    case noViewControllerInNewNodeThatCanBePresented(expectedNewViewControllerToPresent: NewHierarchyNode,
        existingTree: Tree<LiveHierarchyNode>)
    
    case cannotShowNewNodesAfterStory(story: StoryDescription, newNode: NewHierarchyNode)
}

extension HierarchyDescriptionError: Equatable
{
    static func ==(lhs: HierarchyDescriptionError, rhs: HierarchyDescriptionError) -> Bool
    {
        switch (lhs, rhs)
        {
        case (.noNavigationControllerFoundToShowStory, .noNavigationControllerFoundToShowStory):
            return true
            
        case (.reachedEndOfNewNodesNotFoundViewControllerToPresent, .reachedEndOfNewNodesNotFoundViewControllerToPresent):
            return true
            
        case (.noViewControllerInNewNodeThatCanBePresented, .noViewControllerInNewNodeThatCanBePresented):
            return true
            
        case (.cannotShowNewNodesAfterStory, .cannotShowNewNodesAfterStory):
            return true
            
        default:
            return false
        }
    }
}
