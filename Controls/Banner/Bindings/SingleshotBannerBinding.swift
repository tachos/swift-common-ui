//  Created by Ivan Goremykin on 23.05.2018.
//  Copyright © 2018 Tachos. All rights reserved.

//import Auk
//import RxCocoa
//import RxDataSources
//import RxSwift
//import UIKit
//
//// MARK:- Item
//func bind<Item>(_ singleshotDataSourceFacade: SingleshotDataSourceFacade<[Item]>,
//                _ disposeBag: DisposeBag,
//                _ auk: Auk,
//                _ getImageUrlString: @escaping (Item)-> String)
//{
//    singleshotDataSourceFacade.loadedObservable
//        .subscribe {
//            event in
//            
//            auk.removeAll()
//            
//            for item in event.element!
//            {
//                auk.show(url: getImageUrlString(item))
//            }
//            
//        }.disposed(by: disposeBag)
//}
