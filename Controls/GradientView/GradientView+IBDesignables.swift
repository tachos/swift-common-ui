//
//  GradientView+IBDesignables.swift
//  Vegcard
//
//  Created by Vitaly Trofimov on 04/10/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit




@IBDesignable
extension GradientView
{
	@IBInspectable
	var direction: String?
	{
		get
		{
			guard let gradientDirection = gradientDirection
				else
			{
				return nil
			}
			
			switch gradientDirection
			{
			case .none:
				return "none"
			case .horizontal:
				return "horizontal"
			case .vertical:
				return "vertical"
			case .diagonal:
				return "diagonal"
			}
		}
		set
		{
			switch newValue
			{
			case "none":
				gradientDirection = .none
			case "horizontal":
				gradientDirection = .horizontal(
					leftColor: startColor ?? .clear,
					rightColor: endColor ?? .clear
				)
			case "vertical":
				gradientDirection = .vertical(
					bottomColor: startColor ?? .clear,
					topColor: endColor ?? .clear
				)
			case "diagonal":
				gradientDirection = .diagonal(
					topLeftColor: startColor ?? .clear,
					bottomRightColor: endColor ?? .clear
				)
			default:
				gradientDirection = nil
			}
		}
	}
	
	@IBInspectable
	var startColor: UIColor?
	{
		get
		{
			guard let gradientDirection = gradientDirection
				else
			{
				return nil
			}
			
			switch gradientDirection
			{
			case .none:
				return nil
			case .horizontal(let leftColor, _):
				return leftColor
			case .vertical(let bottomColor, _):
				return bottomColor
			case .diagonal(let topLeftColor, _):
				return topLeftColor
			}
		}
		set
		{
			guard let newValue = newValue
				else
			{
				gradientDirection = nil
				
				return
			}
			
			switch gradientDirection
			{
			case .horizontal(_, let rightColor)?:
				gradientDirection = .horizontal(
					leftColor: newValue,
					rightColor: rightColor
				)
			case .vertical(_, let topColor)?:
				gradientDirection = .vertical(
					bottomColor: newValue,
					topColor: topColor
				)
			case .diagonal(_, let bottomRightColor)?:
				gradientDirection = .diagonal(
					topLeftColor: newValue,
					bottomRightColor: bottomRightColor
				)
			default:
				gradientDirection = .vertical(
					bottomColor: newValue,
					topColor: .clear
				)
			}
		}
	}
	
	@IBInspectable
	var endColor: UIColor?
	{
		get
		{
			guard let gradientDirection = gradientDirection
				else
			{
				return nil
			}
			
			switch gradientDirection
			{
			case .none:
				return nil
			case .horizontal(_, let rightColor):
				return rightColor
			case .vertical(_, let topColor):
				return topColor
			case .diagonal(_, let bottomRightColor):
				return bottomRightColor
			}
		}
		set
		{
			guard let newValue = newValue
				else
			{
				gradientDirection = nil
				
				return
			}
			
			switch gradientDirection
			{
			case .horizontal(let leftColor, _)?:
				gradientDirection = .horizontal(
					leftColor: leftColor,
					rightColor: newValue
				)
			case .vertical(let bottomColor, _)?:
				gradientDirection = .vertical(
					bottomColor: bottomColor,
					topColor: newValue
				)
			case .diagonal(let topLeftColor, _)?:
				gradientDirection = .diagonal(
					topLeftColor: topLeftColor,
					bottomRightColor: newValue
				)
			default:
				gradientDirection = .vertical(
					bottomColor: .clear,
					topColor: newValue
				)
			}
		}
	}
}
