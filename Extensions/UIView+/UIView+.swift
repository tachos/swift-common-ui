//
//  UIView+.swift
//  Sellel
//
//  Created by Dmitry Cherednikov on 05/12/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

// MARK:- Corners
extension UIView
{
	func setRoundCorners(radius: CGFloat)
	{
		layer.cornerRadius = radius
		layer.masksToBounds = true
	}
	
    func setRoundCorners(_ corners: UIRectCorner, with radius: CGFloat)
    {
        let borderMask = CAShapeLayer()

        borderMask.frame = bounds
        borderMask.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        
        layer.mask = borderMask
    }
	
	func setMaskedCorners(_ cornersMask: CACornerMask, with radius: CGFloat)
    {
		layer.masksToBounds = true

        if #available(iOS 11.0, *) {
            layer.maskedCorners = cornersMask
        } else {
            // Fallback on earlier versions
        }
		layer.cornerRadius = radius
    }
}

extension CACornerMask
{
	static var layerMinY: CACornerMask = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
	static var layerMaxY: CACornerMask = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
}

// MARK:- Animation
extension UIView
{
    func startRotationAnimation(periodSec: CFTimeInterval = Constants.defaultDownloadAnimationRotationPeriodSec)
    {
        if isRotatingAnimationInProcess
        {
            return
        }
        
        let animation = CABasicAnimation(keyPath: "transform.rotation")

        animation.repeatCount = .infinity
        animation.byValue = 2 * CGFloat.pi
        animation.duration = periodSec

        layer.add(animation, forKey: ViewAnimationKey.infiniteRotation.rawValue)
    }

    func stopRotationAnimation()
    {
        layer.removeAnimation(forKey: ViewAnimationKey.infiniteRotation.rawValue)
    }

    var isRotatingAnimationInProcess: Bool
    {
        let animation = layer.animation(
            forKey: ViewAnimationKey.infiniteRotation.rawValue
        )

        return animation != nil
    }
}

private enum ViewAnimationKey: String
{
    case infiniteRotation = "infiniteRotation"
}

// MARK:- Frames
extension UIView
{
    var globalOrigin: CGPoint?
    {
        return self.superview?.convert(self.frame.origin, to: nil)
    }
    
    var globalFrame: CGRect?
    {
        return self.superview?.convert(self.frame, to: nil)
    }
}

fileprivate struct Constants
{
    static let defaultDownloadAnimationRotationPeriodSec = Double(2)
}

// MARK:- Instantiation
extension UIView
{
    // Using custom view designed in .xib from .storyboard
    // https://stackoverflow.com/a/34524583/1682275
    // https://medium.com/@brianclouser/swift-3-creating-a-custom-view-from-a-xib-ecdfe5b3a960
    @discardableResult
    func addSelfAsSubviewFromXib() -> UIView
    {
        let view = createView(nibName: String(describing: type(of: self)), owner: self)
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.addSubview(view)

        return view
    }
}


// MARK: - Transitions

extension UIView
{
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = 1.0
        }, completion: completion)    }
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options:UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}


extension UIView
{
	func setHidden(
		_ hidden: Bool,
		duration: TimeInterval,
		options: UIView.AnimationOptions = .transitionCrossDissolve,
		completion: ((Bool) -> Void)? = nil
	)
	{
		UIView.transition(
			with: self,
			duration: duration,
			options: options,
			animations:
		{
			self.isHidden = hidden
		},
			completion: completion
		)
	}
}


extension UIView
{
    func setBorder(cornerRadius: CGFloat, borderColor: UIColor, borderWidth: CGFloat)
    {
        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.masksToBounds = true
    }
}

extension UIView
{
    func parentView<T: UIView>(of type: T.Type) -> T?
    {
        guard let view = self.superview else
        {
            return nil
        }
        
        return (view as? T) ?? view.parentView(of: T.self)
    }
	
	func childView<T: UIView>(of type: T.Type) -> T?
    {
		var views = subviews
		
		while let view = views.first
		{
			if let view = view as? T
			{
				return view
			}
			else
			{
				views.removeFirst()
				views.append(contentsOf: view.subviews)
			}
		}
		
		return nil
    }

    func removeAllSuperviewConstraints()
	{
        if let superview = superview
		{
            for constraint in superview.constraints
			{
                if let first = constraint.firstItem as? UIView,
					first == self
				{
                    superview.removeConstraint(constraint)
                }

                if let second = constraint.secondItem as? UIView,
					second == self
				{
                    superview.removeConstraint(constraint)
                }
            }
        }
    }
}



extension UIView
{
	var firstResponder: UIView?
	{
		if isFirstResponder
		{
			return self
		}

		for subview in subviews
		{
			if let firstResponder = subview.firstResponder
			{
				return firstResponder
			}
		}

		return nil
	}
}



extension UIView
{
	static func transparent() -> Self
	{
		let view = Self()
		view.backgroundColor = .clear
		
		return view
	}
}
