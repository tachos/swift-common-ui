//
//  RenderViewToImage.swift
//  Averia Collar
//
//  Created by Ivan Goremykin on 10.09.2018.
//  Copyright © 2018 Averia. All rights reserved.
//

import UIKit

func renderViewToImage(_ view: UIView) -> UIImage?
{
    // This implementation is ~138ms
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
    defer { UIGraphicsEndImageContext() }

    if let context = UIGraphicsGetCurrentContext()
    {
        view.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        return image
    }

    return nil
    
    // This implementation is ~28ms.
    // Doesn't work properly after an Xcode update
    // NEED TO BE CALLED IN viewDidAppear
//    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
//
//        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
//        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()
//
//    UIGraphicsEndImageContext()
//
//    return snapshotImage
}
