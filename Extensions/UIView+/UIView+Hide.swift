//
//  UIView+Hide.swift
//  Hice
//
//  Created by Vitaly Trofimov on 31.08.2021.
//  Copyright © 2021 Hice. All rights reserved.
//

import UIKit




extension Array where Element: UIView
{
	func hide()
	{
		setHidden(true)
	}
	
	func show()
	{
		setHidden(false)
	}
	
	func setHidden(_ hidden: Bool)
	{
		forEach { $0.isHidden = hidden }
	}
}
