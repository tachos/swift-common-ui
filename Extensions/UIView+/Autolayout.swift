//
//  Autolayout.swift
//  Sellel
//
//  Created by Ivan Goremykin on 24.10.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

func activateFullViewStretchConstraints(view: UIView, relativeView: UIView)
{
    NSLayoutConstraint.activate(
    [
        view.leadingAnchor .constraint(equalTo: relativeView.leadingAnchor),
        view.trailingAnchor.constraint(equalTo: relativeView.trailingAnchor),
        view.topAnchor     .constraint(equalTo: relativeView.topAnchor),
        view.bottomAnchor  .constraint(equalTo: relativeView.bottomAnchor),
    ])
}
