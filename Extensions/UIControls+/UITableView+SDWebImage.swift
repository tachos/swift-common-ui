//
//  UITableView+.swift
//
//  Created by Ivan Goremykin on 07.07.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import SDWebImage
import UIKit

extension UITableView
{
    func setImageViewAsTableHeaderView(_ imageResource: ImageResource)
    {
        switch imageResource
        {
        case .local:
            break
            
        case .remote(let url, let placeholderImageName):
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 80))
            imageView.contentMode = .scaleAspectFit
            imageView.sd_setImage(with: url,
                                  placeholderImage: placeholderImageName == nil ? nil : UIImage(named: placeholderImageName!))
            self.tableHeaderView = imageView
        }
    }
}
