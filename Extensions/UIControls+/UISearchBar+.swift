//
//  UISearchBar+.swift
//  Sellel
//
//  Created by Dmitrii Morozov on 14/11/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

extension UISearchBar
{
    func enableCancelButton()
    {
        for view in self.subviews
        {
            for subview in view.subviews
            {
                if let button = subview as? UIButton
                {
                    button.isEnabled = true
                }
            }
        }
    }
    
    func setCancelButtonColor(_ color: UIColor)
    {
        for view in self.subviews
        {
            for subview in view.subviews
            {
                if let button = subview as? UIButton
                {
                    button.setTitleColor(color, for: .normal)
                }
            }
        }
    }
    
    func setCancelButtonTitle(_ title: String)
    {
        for view in self.subviews
        {
            for subview in view.subviews
            {
                if let button = subview as? UIButton
                {
                    button.setTitle(title, for: .normal)
                }
            }
        }
    }
    
    func setTextColor(_ color: UIColor)
    {
        let textFieldInsideSearchBar = value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = color
    }
    
    func setGlassIconViewColor(_ color: UIColor)
    {
        let textFieldInsideSearchBar = value(forKey: "searchField") as? UITextField
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = color
    }
    
    func setTextBackgroundColor(_ color: UIColor)
    {
        let textFieldInsideSearchBar = value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = color
    }
    
    func setPlaceholderColor(_ color: UIColor)
    {
        let textFieldInsideSearchBar = value(forKey: "searchField") as? UITextField
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        
        textFieldInsideSearchBarLabel?.textColor = color
    }
}
