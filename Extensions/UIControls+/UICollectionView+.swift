//
//  UICollectionView+.swift
//  Created by Vitaly Trofimov on 20.07.17.
//

import UIKit

extension UICollectionView
{
	func getCellIfVisible(_ indexPath: IndexPath) -> UICollectionViewCell?
	{
		for visibleCell in visibleCells
		{
			let visibleCellIndexPath = self.indexPath(for: visibleCell)
			
			if visibleCellIndexPath == indexPath
			{
				return visibleCell
			}
		}
		
		return nil
	}
}

extension UICollectionView
{
    func dequeueReusableCell<CellType: UICollectionViewCell>(
		ofType cellType: CellType.Type,
		for indexPath: IndexPath
	) -> CellType?
    {
        let cell = self.dequeueReusableCell(withReuseIdentifier: String(describing: cellType),
                                            for: indexPath) as? CellType
        return cell
    }
    
    func registerNibCell(_ cellIdentifier: String)
    {
        register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    func registerNibCell<CollectionViewCellType: UICollectionViewCell>(
		_ collectionViewCellType: CollectionViewCellType.Type
	)
    {
        registerNibCell(String(describing: collectionViewCellType))
    }
    
	func registerCell<CollectionViewCellType: UICollectionViewCell>(
		_ collectionViewCellType: CollectionViewCellType.Type
	)
	{
		register(collectionViewCellType, forCellWithReuseIdentifier: String(describing: collectionViewCellType))
	}
	
    func registerNibForHeader(_ viewIdentifier: String)
    {
        register(UINib(nibName: viewIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: viewIdentifier)
    }
    
    func registerNibHeader<HeaderViewType: UICollectionReusableView>(
		_ headerViewType: HeaderViewType.Type
	)
    {
        registerNibHeader(HeaderViewType.typeName)
    }
    
    private func registerNibHeader(_ viewIdentifier: String)
    {
        register(UINib(nibName: viewIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: viewIdentifier)
    }
    
    func dequeueSectionHeaderView<CollectionReusableView: UICollectionReusableView>(
		_ headerViewType: CollectionReusableView.Type,
		_ indexPath: IndexPath
	) -> CollectionReusableView
    {
        return dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                withReuseIdentifier: String(describing: headerViewType),
                                                for: indexPath) as! CollectionReusableView
    }
	
	func registerNibFooter<FooterViewType: UICollectionReusableView>(
		_ footerViewType: FooterViewType.Type
	)
	{
		registerNibFooter(FooterViewType.typeName)
	}
	
	private func registerNibFooter(_ viewIdentifier: String)
	{
		register(UINib(nibName: viewIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: viewIdentifier)
	}
	
	func dequeueSectionFooterView<CollectionReusableView: UICollectionReusableView>(
		_ footerViewType: CollectionReusableView.Type, _ indexPath: IndexPath
	) -> CollectionReusableView
	{
		return dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter,
												withReuseIdentifier: String(describing: footerViewType),
												for: indexPath) as! CollectionReusableView
	}
}

// copy-pasted form TableFlip Pod with a few adaptations for UICollectionView
extension UICollectionView
{
    func animateWithTransform(duration:      TimeInterval,
                              transform:     CGAffineTransform,
                              options:       UIView.AnimationOptions = .curveEaseInOut,
                              springDamping: CGFloat = 0.9,
                              completion:    (() -> Void)? = nil)
    {
        for indexPath in indexPathsForVisibleItems
        {
            let delay: TimeInterval = duration / Double(visibleCells.count) * Double(indexPath.item)

            let cell = cellForItem(at: indexPath)!

            cell.layer.setAffineTransform(transform)

            UIView.animate(withDuration: duration,
                           delay: delay,
                           usingSpringWithDamping: springDamping,
                           initialSpringVelocity: 0.0,
                           options: options,
                           animations:
            {
                cell.layer.setAffineTransform(.identity)
            },
                           completion: nil)

            let completionDelay: Int = Int((2 * duration) * 1000)

            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(completionDelay))
            {
                completion?()
            }
        }
    }
}

// MARK:- Reload
extension UICollectionView
{
    func reloadItems(at indexPaths: [IndexPath], animated: Bool)
    {
        let reloadItems =
        {
            self.reloadItems(at: indexPaths)
        }
        
        if animated
        {
            reloadItems()
        }
        else
        {
            UIView.setAnimationsEnabled(false)
            performBatchUpdates(reloadItems, completion:
                {
                    _ in
                    
                    UIView.setAnimationsEnabled(true)
            })
        }
    }
}

// MARK:- Select
extension UICollectionView
{
	func selectItemWithoutAnimation(at indexPath: IndexPath)
	{
		selectItem(at: indexPath, animated: false, scrollPosition: [])
	}

	func deselectItemsWithoutAnimation()
	{
		if let selectedItems = indexPathsForSelectedItems
		{
			for indexPath in selectedItems
			{
				deselectItem(at: indexPath, animated: false)
			}
		}
	}
}
