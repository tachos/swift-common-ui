//
//  UITableView+DataSource.swift
//  Currentus
//
//  Created by Dmitry Cherednikov on 01/02/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit

func findVisibleCell<Item: Equatable>(_ item: Item, _ tableView: UITableView) -> TableViewCell<Item>
{
    for visibleTableViewCell in tableView.visibleCells
    {
        if let tableViewCell = visibleTableViewCell as? TableViewCell<Item>,
            tableViewCell.item == item
        {
            return tableViewCell
        }
    }

    fatalError()
}
