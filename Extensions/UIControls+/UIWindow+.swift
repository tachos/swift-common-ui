//
//  UIWindow+.swift
//
//  Created by Dmitry Cherednikov on 20/06/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit

// Taken from http://www.kekearif.com/how-to-get-the-topmost-viewcontroller/ Rewritten.
extension UIWindow
{
    var topmostViewController: UIViewController?
    {
        if let rootViewController = rootViewController
        {
            return topViewController(for: rootViewController)
        }

        return nil
    }

    private func topViewController(for rootViewController: UIViewController) -> UIViewController
    {
        if let presentedViewController = rootViewController.presentedViewController
        {
            switch presentedViewController
            {
            case is UINavigationController:
                let navigationController = presentedViewController as! UINavigationController
                if let lastViewController = navigationController.viewControllers.last
                {
                    return topViewController(for: lastViewController)
                }

                return navigationController

            case is UITabBarController:
                let tabBarController = presentedViewController as! UITabBarController
                if let selectedViewController = tabBarController.selectedViewController
                {
                    return topViewController(for: selectedViewController)
                }

                return tabBarController

            default:
                return topViewController(for: presentedViewController)
            }
        }

        return rootViewController
    }
}

extension UIWindow
{
	func setRoot(viewController: UIViewController)
	{
		func dismissPresented(
			on viewController: UIViewController?,
			completion: @escaping VoidCallback
		)
		{
			if let viewController = viewController,
				viewController.presentedViewController != nil
			{
				viewController.dismiss(
					animated: false,
					completion: completion
				)
			}
			else
			{
				completion()
			}
		}
		
		dismissPresented(on: rootViewController) {
			self.rootViewController = viewController
		}
	}
}
