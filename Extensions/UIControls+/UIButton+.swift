//
//  UIButton+.swift
//  Sellel
//
//  Created by Ivan Goremykin on 22.11.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit




// MARK: - Title

extension UIButton
{
	var normalTitle: String?
	{
		get
		{
			return title(for: .normal)
		}
		set
		{
			setTitle(newValue, for: .normal)
		}
	}

	var highlightedTitle: String?
	{
		get
		{
			return title(for: .highlighted)
		}
		set
		{
			setTitle(newValue, for: .highlighted)
		}
	}

	var normalAttributedTitle: NSAttributedString?
	{
		get
		{
			return attributedTitle(for: .normal)
		}
		set
		{
			setAttributedTitle(newValue, for: .normal)
		}
	}
}



// MARK: - Image

extension UIButton
{
	var normalImage: UIImage?
	{
		get
		{
			return image(for: .normal)
		}
		set
		{
			setImage(newValue, for: .normal)
		}
	}
	
    func setColoredImage(named imageName: String, color: UIColor)
    {
        let coloredImage = UIImage(named: imageName)!.withRenderingMode(.alwaysTemplate)
        
        setImage(coloredImage, for: .normal)
        tintColor = color
    }

    func setImages(normalImage: UIImage, disabledImage: UIImage)
    {
        setImage(normalImage,   for: .normal)
        setImage(disabledImage, for: .disabled)
    }
}
