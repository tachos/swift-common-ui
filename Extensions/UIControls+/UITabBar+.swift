//  Created by Ivan Goremykin on 19.07.2018.
//  Copyright © 2018 Tachos. All rights reserved.

import UIKit

func removeTabBarItemsText(_ tabBar: UITabBar)
{
    if let items = tabBar.items
    {
        for item in items
        {
            item.title = ""
            item.imageInsets = UIEdgeInsets.init(top: 6, left: 0, bottom: -6, right: 0)
        }
    }
}
