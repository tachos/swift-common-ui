//
//  UITextField+.swift
//  FoodPulse
//
//  Created by Ivan Goremykin on 08.02.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit

func anyTextFieldIsNilOrEmpty(_ textFields: UITextField...) -> Bool
{
    return anyTextFieldIsNilOrEmpty(textFields)
}

func anyTextFieldIsNilOrEmpty(_ textFields: [UITextField]) -> Bool
{
    return any(isNilOrEmpty, textFields.map { $0.text })
}
