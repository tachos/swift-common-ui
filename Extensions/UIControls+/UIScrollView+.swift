//
//  UIScrollView+.swift
//  Broadway
//
//  Created by Dmitry Cherednikov on 17/07/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit

extension UIScrollView
{
    func scrollToBottom(animated: Bool)
    {
        if contentSize.height > bounds.size.height
        {
            let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height)

            setContentOffset(bottomOffset, animated: animated)
        }
    }
}
