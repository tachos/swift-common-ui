//
//  UISearchBar+Rx.swift
//
//  Created by Dmitrii Morozov on 14/11/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import RxSwift
import UIKit

// https://medium.com/@freak4pc/whats-new-in-rxswift-5-f7a5c8ee48e7

func createSearchBarTextObservable(_ searchBar: UISearchBar) -> Observable<String>
{
    return searchBar.rx.text
        .orEmpty
        .skip(1)
        .throttle(.milliseconds(300), scheduler: MainScheduler.instance)
        .filter{ !$0.isEmpty }
        .map(sanitizeWhitespaces)
        .distinctUntilChanged()
        .observeOn(MainScheduler.instance)
}

func createUnfilteredSearchBarTextObservable(_ searchBar: UISearchBar) -> Observable<String>
{
    return searchBar.rx.text
        .orEmpty
        .skip(1)
        .throttle(.milliseconds(300), scheduler: MainScheduler.instance)
        .map(sanitizeWhitespaces)
        .distinctUntilChanged()
        .observeOn(MainScheduler.instance)
}
