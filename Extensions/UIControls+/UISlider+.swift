//
//  UISlider+.swift
//  Sellel
//
//  Created by Dmitry Cherednikov on 20/12/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

extension UISlider
{
    func hideThumb()
    {
        setThumbImage(UIImage(), for: .normal)
    }
}
