//
//  UITableViewCell+.swift
//  Sellel
//
//  Created by Dmitry Cherednikov on 09/02/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit

extension UITableViewCell
{
    func localize(_ string: String) -> String
    {
        return string.localized(using: "Cells")
    }
}

// MARK:- Table Views with Dynamic Cell Height
extension UITableViewCell
{
    func computeHeight(in tableView: UITableView) -> CGFloat
    {
        dbgcheck(tableView.frame.width > 0)
        let size = systemLayoutSizeFitting(CGSize(width: tableView.frame.width,
                                                  height: .greatestFiniteMagnitude))
        return size.height
    }
}
