//
//  UIImage+SDWebImage.swift
//
//  Created by Dmitrii Morozov on 22/11/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import SDWebImage

func setupImageView(_ imageView: UIImageView, normalImageUrl: URL, selectedImageUrl: URL, placeholderImage: UIImage, selectedImageCompletion: VoidCallback? = nil)
{
    imageView.sd_setImage(with: normalImageUrl, placeholderImage: placeholderImage)
    imageView.sd_setHighlightedImage(with: selectedImageUrl, options: [], completed: { _, _, _, _ in selectedImageCompletion?() })
}
