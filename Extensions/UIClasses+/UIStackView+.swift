//
//  UIStackView+.swift
//  Created by Dmitry Cherednikov on 05.05.2022.
//

import UIKit



extension UIStackView
{
	convenience init(
		_ axis: NSLayoutConstraint.Axis,
		distribution: Distribution = .fill,
		alignment: Alignment = .fill,
		spacing: CGFloat = 0,
		arrangedSubviews: [UIView] = []
	)
	{
		self.init(arrangedSubviews: arrangedSubviews)
		
		self.axis = axis
		self.distribution = distribution
		self.alignment = alignment
		self.spacing = spacing
	}
}
