//
//  UIColor+.swift
//  FoodPulse
//
//  Created by Dmitry Cherednikov on 06/03/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit

extension UIColor
{
    static var random: UIColor
    {
        return UIColor(red:   .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue:  .random(in: 0...1),
                       alpha: 1.0)
    }
}

extension UIColor
{
    var redComponent: CGFloat
    {
        return getRGB().red
    }

    var greenComponent: CGFloat
    {
        return getRGB().green
    }

    var blueComponent: CGFloat
    {
        return getRGB().blue
    }
	
	var alpha: CGFloat
	{
		return getRGBA().alpha
	}

    private func getRGB() -> (red: CGFloat, green: CGFloat, blue: CGFloat)
    {
        var red = CGFloat()
        var green = CGFloat()
        var blue = CGFloat()
        var alpha = CGFloat()

        if getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        {
            return (red, green, blue)
        }
        else
        {
            return (0, 0, 0)
        }
    }
	
	func getRGBA() -> (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)
	{
		var red = CGFloat()
		var green = CGFloat()
		var blue = CGFloat()
		var alpha = CGFloat()
		
		if getRed(&red, green: &green, blue: &blue, alpha: &alpha)
		{
			return (red, green, blue, alpha)
		}
		else
		{
			return (0, 0, 0, 0)
		}
	}
}

extension UIColor
{
    var hexString: String
    {
        let (red, green, blue, alpha) = getRGBA()

        return String(format: "#%02lX%02lX%02lX%02lX",
                      Int(red * 255),
                      Int(green * 255),
                      Int(blue * 255),
                      Int(alpha * 255))
    }

    public convenience init?(hex: String)
    {
        if let (r, g, b, a) = UIColor.componentsFromHex(hex)
        {
            self.init(red: r, green: g, blue: b, alpha: a)
        }
        else
		{
        	return nil
		}
    }
	
	public convenience init?(displayP3hex hex: String, setAlpha alpha: CGFloat? = nil)
	{
		if let (r, g, b, a) = UIColor.componentsFromHex(hex)
		{
			self.init(displayP3Red: r, green: g, blue: b, alpha: alpha ?? a)
		}
		else
		{
			return nil
		}
	}
	
	private static func componentsFromHex(_ hex: String) -> (r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat)?
	{
		// Sanitize "#" character
		let offset = hex.hasPrefix("#") ? 1 : 0
		let start = hex.index(hex.startIndex, offsetBy: offset)
		let sanitizedNumberSignColor = String(hex[start...]).lowercased()
		
		// Sanitize alpha channel
		let hexColor = sanitizedNumberSignColor.count == 8
			? sanitizedNumberSignColor
			: sanitizedNumberSignColor + "ff"
		
		if hexColor.count == 8
		{
			let scanner = Scanner(string: hexColor)
			var hexNumber: UInt64 = 0
			
			if scanner.scanHexInt64(&hexNumber)
			{
				let r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
				let g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
				let b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
				let a = CGFloat(hexNumber & 0x000000ff) / 255
				
				return (r, g, b, a)
			}
		}
		
		return nil
	}
	
	public static func interpolated(
		between startColor: UIColor,
		and endColor: UIColor,
		fraction: CGFloat
	) -> UIColor
	{
		return UIColor(
			red: lerpFloat(
				between: startColor.redComponent,
				and: endColor.redComponent,
				fraction: fraction
			),
			green: lerpFloat(
				between: startColor.greenComponent,
				and: endColor.greenComponent,
				fraction: fraction
			),
			blue: lerpFloat(
				between: startColor.blueComponent,
				and: endColor.blueComponent,
				fraction: fraction
			),
			alpha: lerpFloat(
				between: startColor.alpha,
				and: endColor.alpha,
				fraction: fraction
			)
		)
	}
}


private func lerpFloat(
	between startValue: CGFloat,
	and endValue: CGFloat,
	fraction: CGFloat
) -> CGFloat
{
	return startValue + (endValue - startValue) * fraction
}
