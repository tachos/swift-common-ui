//
//  UIFont+.swift
//
//  Created by Dmitry Cherednikov on 23.03.2022.
//  Copyright © 2022 Tachos. All rights reserved.
//

import UIKit



extension UIFont
{
	func withMonospacedDigits() -> UIFont
	{
		return UIFont(
			descriptor: fontDescriptor.addingAttributes(
				[
					.featureSettings: [
						[
							UIFontDescriptor.FeatureKey.featureIdentifier: kNumberSpacingType,
							UIFontDescriptor.FeatureKey.typeIdentifier: kMonospacedNumbersSelector
						]
					]
				]
			),
			size: pointSize
		)
	}
}
