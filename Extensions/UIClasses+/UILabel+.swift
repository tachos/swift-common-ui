//
//  UILabel+.swift
//  Hice
//
//  Created by Dmitry Cherednikov on 19.02.2021.
//  Copyright © 2021 Hice. All rights reserved.
//

import UIKit



extension UILabel
{
	func setLineHeight(_ lineHeight: CGFloat)
	{
		let paragraphStyle = NSMutableParagraphStyle()
		paragraphStyle.minimumLineHeight = lineHeight
		paragraphStyle.maximumLineHeight = lineHeight

		var newAttributedText: NSMutableAttributedString?

		if let attributedText = attributedText
		{
			newAttributedText = NSMutableAttributedString(
				attributedString: attributedText
			)
		}
		else if let text = text
		{
			newAttributedText = NSMutableAttributedString(
				string: text
			)
		}

		if let newAttributedText = newAttributedText
		{
			newAttributedText.addAttribute(
				.paragraphStyle,
				value: paragraphStyle,
				range: NSRange(location: 0, length: newAttributedText.string.count)
			)

			self.attributedText = newAttributedText
		}
	}
}
