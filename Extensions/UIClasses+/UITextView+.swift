//
//  UITextView+.swift
//  Hice
//
//  Created by Dmitry Cherednikov on 03.11.2022.
//  Copyright © 2022 Hice. All rights reserved.
//

import UIKit 



extension UITextView
{
	func performPreservingCaretPosition(_ edit: () -> Void)
	{
		let previousSelectedTextRange = selectedTextRange
		let previousText = text ?? ""
		
		edit()
		
		let deletedSymbolsCount = max(
			0,
			previousText.count - text.count
		)
		if let previousSelectedTextRange = previousSelectedTextRange,
		   let position = position(
				from: previousSelectedTextRange.end,
				offset: -deletedSymbolsCount
		   )
		{
			selectedTextRange = textRange(
				from: position,
				to: position
			)
		}
	}
}
