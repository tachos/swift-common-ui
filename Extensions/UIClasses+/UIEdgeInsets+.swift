//
//  UIEdgeInsets+.swift
//  Hice
//
//  Created by Dmitry Cherednikov on 29.04.2021.
//  Copyright © 2021 Hice. All rights reserved.
//

import UIKit



extension UIEdgeInsets
{
	var vertical: CGFloat
	{
		return top + bottom
	}

	var horizontal: CGFloat
	{
		return right + left
	}

	static func make(
		top: CGFloat = 0,
		left: CGFloat = 0,
		bottom: CGFloat = 0,
		right: CGFloat = 0
	) -> UIEdgeInsets
	{
		return .init(
			top: top,
			left: left,
			bottom: bottom,
			right: right
		)
	}
}
