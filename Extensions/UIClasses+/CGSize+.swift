//  Created by Ivan Goremykin on 18/04/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import UIKit

func + (left: CGSize, right: CGSize) -> CGSize
{
    return CGSize(width: left.width + right.width, height: left.height + right.height)
}

func * (size: CGSize, scale: CGFloat) -> CGSize
{
    return CGSize(width: size.width * scale, height: size.height * scale)
}

func / (size: CGSize, scale: CGFloat) -> CGSize
{
    return CGSize(width: size.width / scale, height: size.height / scale)
}

func fitRect(_ rect: CGRect, currentValue: CGFloat, desiredValue: CGFloat) -> (scaledRect: CGRect, scale: CGFloat)
{
    let scale = currentValue / desiredValue
    
    let scaledSize = rect.size / scale
    let scaledRect = CGRect(center: rect.center, size: scaledSize)
    
    return (scaledRect: scaledRect, scale: scale)
}



extension CGSize
{
	init(side: CGFloat)
	{
		self.init(width: side, height: side)
	}
}
