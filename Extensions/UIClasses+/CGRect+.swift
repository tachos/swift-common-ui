//
//  CGRect+.swift
//  Sellel
//
//  Created by Ivan Goremykin on 20.10.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

extension CGRect
{
    init(center: CGPoint, size: CGSize)
    {
        let originX = center.x - size.width/2
        let originY = center.y - size.height/2
        let origin = CGPoint(x: originX, y: originY)
        
        self.init(origin: origin, size: size)
    }
    
    func centerRelativeToOrigin() -> CGPoint
    {
        return CGPoint(x: self.midX, y: self.midY)
    }
}

extension CGRect
{
    var center: CGPoint
    {
        return CGPoint(x: midX, y: midY)
    }
    
    var min: CGPoint
    {
        return CGPoint(x: minX, y: minY)
    }
    
    var max: CGPoint
    {
        return CGPoint(x: maxX, y: maxY)
    }
    
    var area: CGFloat
    {
        return width * height
    }
}

func + (rect: CGRect, size: CGSize) -> CGRect
{
    let newSize = CGSize(width: rect.size.width + size.width, height: rect.size.height + size.height)

    return CGRect(center: rect.center, size: newSize)
}

func += (rect: inout CGRect, size: CGSize)
{
    rect = rect + size
}
