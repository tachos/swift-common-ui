//
//  Scroll.swift
//  Averia Collar
//
//  Created by Dmitry Cherednikov on 10/12/2018.
//  Copyright © 2018 Averia. All rights reserved.
//

import UIKit

func disableAutomaticInsetsAdjustment(scrollView: UIScrollView, viewController: UIViewController)
{
    if #available(iOS 11, *)
    {
        scrollView.contentInsetAdjustmentBehavior = .never
    }
    else
    {
        viewController.automaticallyAdjustsScrollViewInsets = false
    }
}
