//
//  UIPanGestureRecognizer+.swift
//  Sellel
//
//  Created by Ivan Goremykin on 19.10.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

extension UIPanGestureRecognizer
{
    func resetAccumulatedTranslation(in view: UIView)
    {
        setTranslation(CGPoint(x: 0, y: 0), in: view)
    }
}
