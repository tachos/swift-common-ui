//
//  UIResponder+.swift
//  Sellel
//
//  Created by Dmitrii Morozov on 08/11/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

extension UIResponder
{
    func next<Type: UIResponder>(_ type: Type.Type) -> Type?
    {
        return next as? Type ?? next?.next(type)
    }
}
