//
//  UITextField+ReactiveX.swift
//  Averia Collar
//
//  Created by Ivan Goremykin on 22/11/2018.
//  Copyright © 2018 Averia. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

class RxTextFieldDelegateProxy: DelegateProxy<UITextField, UITextFieldDelegate>, DelegateProxyType, UITextFieldDelegate
{
    init(textField: UITextField) {
        super.init(parentObject: textField, delegateProxy: RxTextFieldDelegateProxy.self)
    }
    
    static func registerKnownImplementations() {
        self.register { RxTextFieldDelegateProxy(textField: $0)}
    }
    
    static func currentDelegate(for object: UITextField) -> UITextFieldDelegate? {
        return object.delegate
    }
    
    static func setCurrentDelegate(_ delegate: UITextFieldDelegate?, to object: UITextField) {
        object.delegate = delegate
    }
}

extension Reactive where Base: UITextField {
    
    public var delegate: DelegateProxy<UITextField, UITextFieldDelegate> {
        return RxTextFieldDelegateProxy.proxy(for: base)
    }
    
    public var editingDidBegin: Observable<Void>
    {
        return delegate.sentMessage(#selector(UITextFieldDelegate.textFieldDidBeginEditing(_:))).map { value in
            return ()   }
    }
    
    public var editingDidChange: Observable<String>
    {
        return controlEvent([.editingChanged]).asObservable().map { [weak base] in base?.text ?? "" }
    }
    
    public var editingDidEnd: Observable<String> {
        return delegate.sentMessage(#selector(UITextFieldDelegate.textFieldDidEndEditing(_:))).map { value in
            guard let textField = value[0] as? UITextField else { return "" }
            return textField.text!   }
    }
    
}
