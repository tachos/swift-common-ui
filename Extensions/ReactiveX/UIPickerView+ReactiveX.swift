//
//  UIPickerView+ReactiveX.swift
//  Broadway
//
//  Created by Ivan Goremykin on 15/07/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import RxSwift
import UIKit

@discardableResult
func setupItemPickerInput(itemTitles: [String],
                          selectedIndex: Int?,
                          handleEditingDidEnd: @escaping IntCallback,
                          _ disposeBag: DisposeBag,
                          _ textField:  UITextField,
                          _ pickerView: UIPickerView) -> BehaviorSubject<[String]>
{
    let subject = setupPicker(itemTitles, selectedIndex, disposeBag, textField, pickerView)
    
    bindTextFieldToPickerView(itemTitles, disposeBag, textField, pickerView)
    setupEditingDidEndHandler(handleEditingDidEnd, disposeBag, textField, pickerView)
    
    return subject
}

private func setupPicker(_ itemTitles: [String], _ selectedIndex: Int?, _ disposeBag: DisposeBag, _ textField: UITextField, _ pickerView: UIPickerView) -> BehaviorSubject<[String]>
{
    pickerView.delegate = nil
    pickerView.dataSource = nil
    
    textField.inputView = pickerView
    
    let subject = populatePickerView(itemTitles, disposeBag, pickerView)
    selectPickerViewRowIfNeeded(selectedIndex, pickerView)
    
    return subject
}

private func populatePickerView(_ itemTitles: [String], _ disposeBag: DisposeBag, _ pickerView: UIPickerView) -> BehaviorSubject<[String]>
{
    let behaviorSubject = BehaviorSubject(value: itemTitles)
    
    behaviorSubject
        .bind(to: pickerView.rx.itemTitles) { _, item in return item }
        .disposed(by: disposeBag)
    
    return behaviorSubject
}

private func selectPickerViewRowIfNeeded(_ selectedIndex: Int?, _ pickerView: UIPickerView)
{
    if let selectedIndex = selectedIndex
    {
        pickerView.selectRow(selectedIndex, inComponent: 0, animated: false)
    }
}

private func bindTextFieldToPickerView(_ itemTitles: [String], _ disposeBag: DisposeBag, _ textField:  UITextField, _ pickerView: UIPickerView)
{
    // Update text field on picker view select.
    pickerView.rx.modelSelected(String.self)
        .subscribe(onNext:
            {
                [weak textField] model in
                
                textField?.text = model.first!
        })
        .disposed(by: disposeBag)
    
    // In case picker was not even touched, we still have to update the text field on editing did end.
    //    textField.rx.editingDidEnd
    //        .subscribe(onNext:
    //        {
    //            [weak pickerView, weak textField] _ in
    //
    //            if let pickerView = pickerView, let textField = textField
    //            {
    //                let selectedIndex = pickerView.selectedRow(inComponent: 0)
    //                textField.text = itemTitles[selectedIndex]
    //            }
    //
    //        })
    //        .disposed(by: disposeBag)
}

private func setupEditingDidEndHandler(_ handleEditingDidEnd: @escaping IntCallback,
                                       _ disposeBag: DisposeBag,
                                       _ textField:  UITextField,
                                       _ pickerView: UIPickerView)
{
    textField.rx.editingDidEnd
        .subscribe(onNext:
            {
                [weak pickerView] _ in
                
                if let pickerView = pickerView
                {
                    handleEditingDidEnd(pickerView.selectedRow(inComponent: 0))
                    
                }
        })
        .disposed(by: disposeBag)
}
