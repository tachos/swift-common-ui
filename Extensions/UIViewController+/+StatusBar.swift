//
//  +StatusBar.swift
//  Sellel
//
//  Created by Dmitry Cherednikov on 09/01/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit

extension UIViewController
{
    func setStatusBarBackgroundColor(_ color: UIColor)
    {
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView
        {
            statusBar.backgroundColor = color 
        }
    }
}
