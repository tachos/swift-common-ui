//
//  +SafeArea.swift
//  Broadway
//
//  Created by Vyacheslav Shakaev on 27/07/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit

extension UIViewController
{
    var safeAreaTop: CGFloat
    {
        if #available(iOS 11.0, *) {
            return self.view.safeAreaInsets.top
        } else {
            return self.topLayoutGuide.length
        }
    }
    
    var safeAreaBottom: CGFloat
    {
        if #available(iOS 11.0, *) {
            return self.view.safeAreaInsets.bottom
        } else {
            return self.bottomLayoutGuide.length
        }
    }
}
