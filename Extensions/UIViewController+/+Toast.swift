//
//  UIViewController+Toast.swift
//  Sellel
//
//  Created by Dmitrii Morozov on 21/11/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import Toast_Swift

// MARK:- Toast
extension UIViewController
{
    fileprivate struct Constants
    {
        static let defaultToastDuration: Double  = 3.0
    }
    
    func showToastAtTop(title: String, messageColor: UIColor = .white, backgroundColor: UIColor = .black)
    {
        var style = ToastStyle()
        
        style.messageColor    = messageColor
        style.backgroundColor = backgroundColor
        
        self.view.makeToast(title, duration: Constants.defaultToastDuration, position: .top, style: style)
    }
    
    func hideActiveToast()
    {
        self.view.hideToast()
    }
    
    func toast(_ message: String)
    {
        var style = ToastStyle()
        
        style.titleAlignment = .center
        
        view.makeToast(message, duration: Constants.defaultToastDuration, position: .bottom, style: style)
    }
    
    func toast(_ message: String, _ position: ToastPosition)
    {
        var style = ToastStyle()
        
        style.titleAlignment = .center
        
        view.makeToast(message, duration: Constants.defaultToastDuration, position: position, style: style)
    }
    
    func showToastAtTop(_ message: String)
    {
        showToastAtTop(title: message)
    }
}

extension UIViewController
{
    func showActivityAndDisableInteraction()
    {
        view.isUserInteractionEnabled = false
        view.makeToastActivity(.center)
    }
    
    func hideActivityAndEnableInteraction()
    {
        view.isUserInteractionEnabled = true
        view.hideToastActivity()
    }
}
