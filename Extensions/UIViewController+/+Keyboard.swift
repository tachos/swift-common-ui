//
//  UIViewController+Keyboard.swift
//  Sellel
//
//  Created by Dmitrii Morozov on 21/11/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit
import RxSwift

extension UIViewController
{
    func registerForKeyboardDidShowNotification(tableView: UITableView, handler: ((CGSize?) -> Void)? = nil)
    {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardDidShowNotification, object: nil, queue: nil)
        {
            (notification) -> Void in
            
            let userInfo = notification.userInfo!
            let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size
            let contentInsets = UIEdgeInsets.init(top: tableView.contentInset.top, left: tableView.contentInset.left, bottom: keyboardSize.height, right: tableView.contentInset.right)
            
            tableView.setContentInsetAndScrollIndicatorInsets(contentInsets)
            
            handler?(keyboardSize)
        }
    }
    
    func registerForKeyboardWillHideNotification(tableView: UITableView, handler: (() -> Void)? = nil)
    {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil)
        {
            (notification) -> Void in
            
            let contentInsets = UIEdgeInsets.init(top: tableView.contentInset.top, left: tableView.contentInset.left, bottom: 0, right: tableView.contentInset.right)
            
            tableView.setContentInsetAndScrollIndicatorInsets(contentInsets)
            
            handler?()
        }
    }
}

extension UIViewController
{
    func animateUIOnKeyboardEvents(updateUI: @escaping (CGFloat) -> Void, disposeBag: DisposeBag)
    {
        subscribeOnNext(NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification),
                        br(w(self, type(of: self).animateUIOnKeyboardWillShow), updateUI),
                        disposeBag)

        subscribeOnNext(NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification),
                        br(w(self, type(of: self).animateUIOnKeyboardWillHide), updateUI),
                        disposeBag)
    }

    private func animateUIOnKeyboardWillShow(_ notification: Notification, _ updateUI: @escaping (CGFloat) -> Void)
    {
        let userInfo = notification.userInfo

        let animationDuration: TimeInterval = (userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval) ?? 0
        let keyboardHeight = (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.size.height ?? 0

        UIView.animate(withDuration: animationDuration, animations:
        {
            updateUI(keyboardHeight)

            self.view.layoutIfNeeded()
        })
    }

    private func animateUIOnKeyboardWillHide(_ notification: Notification, _ updateUI: @escaping (CGFloat) -> Void)
    {
        let userInfo = notification.userInfo

        let animationDuration: TimeInterval = (userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval) ?? 0

        UIView.animate(withDuration: animationDuration, animations:
        {
            updateUI(0)

            self.view.layoutIfNeeded()
        })
    }
}
