//
//  UIViewController+ToolBar.swift
//  Sellel
//
//  Created by Dmitrii Morozov on 25/10/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

extension UIViewController
{
    func toolBar(title: String, doneButtonCallback: Selector, cancelButtonCallback: Selector) -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: localizeString("Done"), style: .plain, target: self, action: doneButtonCallback)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: localizeString("Cancel"), style: .plain, target: self, action: cancelButtonCallback)
        
        let barLabel = createBarLabel(title: title)
        
        toolBar.setItems([cancelButton, spaceButton, barLabel, spaceButton, doneButton], animated: false)
        
        return toolBar
    }
    
    private func createBarLabel(title: String) -> UIBarButtonItem
    {
        let label = UILabel()
        label.text = title
        
        return UIBarButtonItem(customView:label)
    }
}
