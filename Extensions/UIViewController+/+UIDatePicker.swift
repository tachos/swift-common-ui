//
//  UIViewController+UIDatePicker.swift
//  Sellel
//
//  Created by Dmitrii Morozov on 25/10/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

// MARK:- UIDatePicker
extension UIViewController
{
    func datePicker(handler: Selector, mode: UIDatePicker.Mode = .date, date: Date = Date()) -> UIDatePicker
    {
        let datePicker = UIDatePicker()
        
        datePicker.timeZone        = NSTimeZone.local
        datePicker.backgroundColor = UIColor.white
        datePicker.datePickerMode  = mode
        datePicker.date            = date
        
        datePicker.addTarget(self, action: handler, for: .valueChanged)
        
        return datePicker
    }
}
