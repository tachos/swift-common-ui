//
//  +TextInputDialog.swift
//  Sellel
//
//  Created by Ivan Goremykin on 16.01.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit

extension UIViewController
{
    func showTextInputDialog(title:                   String,
                             message:                 String,
                             textPlaceholder:         String,
                             validate:                @escaping ValidateCallback,
                             confirmButtonTitle:      String,
                             cancelButtonTitle:       String,
                             onConfirm handleConfirm: @escaping (String) -> Void,
                             onCancel handleCancel:   VoidCallback? = nil)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: confirmButtonTitle, style: .default)
        {
            _ in
            
            let text = alertController.textFields![0].text ?? ""
            
            switch validate(text)
            {
            case .ok:
                handleConfirm(text)
                
            case .notOk(let description):
                // TODO implement shaking behaviour
                // https://github.com/Orderella/PopupDialog
                self.alert(title: "Wrong text", message: description)
            }
        }
        
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { (_) in }
        
        alertController.addTextField
        {
            textField in
            
            textField.placeholder = textPlaceholder
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
}
