//
//  +ToastActivity.swift
//  Broadway
//
//  Created by Ivan Goremykin on 02/08/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Toast_Swift

// MARK:- Methods
extension UIViewController
{
    func toastActivityAndDisableView()
    {
        view.isUserInteractionEnabled = false
        view.makeToastActivity(.center)
    }
    
    func hideToastActivityAndEnableView()
    {
        view.isUserInteractionEnabled = true
        view.hideToastActivity()
    }
}

// MARK:- Callbacks
extension UIViewController
{
    func toastActivityAndDisableViewCallback() -> VoidCallback
    {
        return w(self, UIViewController.toastActivityAndDisableView)
    }
    
    func hideToastActivityAndEnableViewCallback() -> VoidCallback
    {
        return w(self, UIViewController.hideToastActivityAndEnableView)
    }
    
    func toastCallback(_ message: String) -> VoidCallback
    {
        return {
            [weak self] in
            
            self?.toast(message)
        }
    }
    
    func alertCallback(_ message: String) -> VoidCallback
    {
        return {
            [weak self] in
            
            self?.alert(title: "", message: message)
        }
    }
}
