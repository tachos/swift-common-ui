//
//  +PopSelfAnimated.swift
//  Broadway
//
//  Created by Ivan Goremykin on 05/07/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit

extension UIViewController
{
    func alertAndPopSelfAnimated(title: String, message: String)
    {
        alert(title: title,  message: message, buttonCallback: popSelfAnimated)
    }
    
    var popSelfAnimated: VoidCallback
    {
        return { [weak self] in self?.navigationController?.popViewController(animated: true) }
    }
}

extension UIViewController
{
    func alertAndPopSelfAnimatedCallback(_ title: String, _ message: String) -> VoidCallback
    {
        return {
            [weak self] in
            
            self?.alertAndPopSelfAnimated(title: title, message: message)
        }
    }
}
