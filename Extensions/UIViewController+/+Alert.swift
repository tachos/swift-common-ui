//
//  UIViewController+Alert.swift
//  StarHub
//
//  Created by Ivan Goremykin on 24.05.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

extension UIViewController
{
    @discardableResult
    func alert(title: String, message: String) -> UIViewController
    {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: localizeString("ok"), style: UIAlertAction.Style.cancel, handler: nil))
        
        DispatchQueue.main.async
        {
            self.present(alertController, animated: true, completion: nil)
        }
        
        return alertController
    }
    
    func alert(errorText: ErrorText)
    {
        alert(title: errorText.errorTitle, message: errorText.errorDescription)
    }
    
    func alert(alertText: AlertText)
    {
        alert(title: alertText.title, message: alertText.message)
    }
    
    @discardableResult
    func alert(title: String, message: String, buttonText: String = "Ok", buttonCallback: @escaping () -> Void, onCompletion handleCompletion: VoidCallback? = nil) -> UIViewController
    {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: UIAlertController.Style.alert)
        
        let action = UIAlertAction(title: buttonText, style: UIAlertAction.Style.default)
        {
            _ in
            
            buttonCallback()
        }
        
        alertController.addAction(action)
        
        self.present(alertController, animated: true, completion: handleCompletion)
        
        return alertController
    }
    
    func leftRightAlert(title: String,
                        message: String,
                        leftButtonText:  String, leftCallback:  @escaping () -> Void,
                        rightButtonText: String, rightCallback: @escaping () -> Void)
    {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: UIAlertController.Style.alert)
        
        let leftAction = UIAlertAction(title: leftButtonText, style: UIAlertAction.Style.default)
        {
            _ in
            
            leftCallback()
        }
        
        let rightAction = UIAlertAction(title: rightButtonText, style: UIAlertAction.Style.default)
        {
            _ in
            
            rightCallback()
        }
        
        alertController.addAction(leftAction)
        alertController.addAction(rightAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func yesNoAlert(title: String,
                    message: String,
                    leftButtonText: String = "No",
                    rightButtonText: String = "Yes",
                    yesCallback: @escaping () -> Void)
    {
        leftRightAlert(title: title,
                       message: message,
                       leftButtonText:  leftButtonText,  leftCallback: {},
                       rightButtonText: rightButtonText, rightCallback: yesCallback)
    }
}

struct AlertText
{
    let title: String
    let message: String
}
