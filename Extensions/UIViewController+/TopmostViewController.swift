//
//  TopmostViewController.swift
//  BBC
//
//  Created by Dmitry Cherednikov on 20/06/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit

var topmostViewController: UIViewController
{
    return UIApplication.shared.keyWindow!.topmostViewController!
}
