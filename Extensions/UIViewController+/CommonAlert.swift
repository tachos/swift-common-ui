//
//  CommonAlert.swift
//  BBC
//
//  Created by Dmitry Cherednikov on 20/06/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

func alertOnTopMostViewController(title: String = "", message: String)
{
    topmostViewController.alert(title: title, message: message)
}
