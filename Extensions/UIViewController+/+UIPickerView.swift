//
//  UIViewController+UIPickerView.swift
//  Sellel
//
//  Created by Dmitrii Morozov on 25/10/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

// MARK:- UIPickerView
extension UIViewController
{
    func pickerView() -> UIPickerView
    {
        let pickerView = UIPickerView()
        
        pickerView.delegate = self as? UIPickerViewDelegate
        pickerView.dataSource = self as? UIPickerViewDataSource
        
        return pickerView
    }
}
