//
//  UIViewController+Freeze.swift
//  Sellel
//
//  Created by Ivan Goremykin on 29.08.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import Toast_Swift
import UIKit

extension UIViewController
{
    func freezeUI(_ position: ToastPosition = .center)
    {
        if !UiFreezeState.freezedViewControllers.contains(self)
        {
            UiFreezeState.freezedViewControllers.insert(self)
            
            view.isUserInteractionEnabled = false
            view.makeToastActivity(position)
        }
    }
    
    func unfreezeUI()
    {
        if UiFreezeState.freezedViewControllers.contains(self)
        {
            UiFreezeState.freezedViewControllers.remove(self)
            
            view.isUserInteractionEnabled = true
            view.hideToastActivity()
        }
    }
    
    func unfreezeUI(unfreezeInteraction: Bool)
    {
        view.isUserInteractionEnabled = unfreezeInteraction
        
        unfreezeUI()
    }
    
    func unfreezeUI(alertTitle: String, alertMessage: String)
    {
        unfreezeUI()
        
        alert(title: alertTitle, message: alertMessage)
    }
    
    private class UiFreezeState
    {
        static var freezedViewControllers: Set<UIViewController> = Set()
    }
}
