//
//  UIViewController+Views.swift
//  Sellel
//
//  Created by Ivan Goremykin on 07.09.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

// MARK:- Show / Hide
func show(_ views: [UIView?])
{
    views.forEach{ $0?.isHidden = false }
}

func hide(_ views: [UIView?])
{
    views.forEach{ $0?.isHidden = true }
}

// MARK:- Show / Hide
func showAlpha(_ views: [UIView?], _ duration: TimeInterval = 1.0)
{
    views.forEach{ $0?.fadeIn(duration) }
}

func hideAlpha(_ views: [UIView?])
{
    views.forEach{ $0?.alpha = 0 }
}

// MARK:- Enable / Disable User Interaction
extension UIViewController
{
    func enableUserInteraction(_ views: [UIView?])
    {
        views.forEach{ $0?.isUserInteractionEnabled = true }
    }
    
    func disableUserInteraction(_ views: [UIView?])
    {
        views.forEach{ $0?.isUserInteractionEnabled = false }
    }
}

// MARK:- View Visibility
extension UIViewController
{
    var isViewVisible: Bool
    {
        return isViewLoaded && view.window != nil
    }
}
