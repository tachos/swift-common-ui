//
//  +NavigationBar.swift
//  Averia Collar
//
//  Created by Dmitry Cherednikov on 11/12/2018.
//  Copyright © 2018 Averia. All rights reserved.
//

import RxSwift
import RxCocoa

extension UIViewController
{
    func addRigthButtonToNavigationBar(title: String, color: UIColor, action: @escaping VoidCallback, disposeBag: DisposeBag)
    {
        let item = UIBarButtonItem(title: title, style: .plain, target: nil, action: nil)
        item.tintColor = color

        item.rx.tap
            .subscribe(onNext: action)
            .disposed(by: disposeBag)

        navigationItem.rightBarButtonItem = item
    }
}
