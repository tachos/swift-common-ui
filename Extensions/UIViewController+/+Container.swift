//
//  Container.swift
//  Sellel
//
//  Created by Ivan Goremykin on 18.01.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import UIKit

func embedView(containerView: UIView, embeddedView: UIView, intoSafeArea: Bool = false)
{
    embeddedView.translatesAutoresizingMaskIntoConstraints = false
    containerView.addSubview(embeddedView)
    
    if intoSafeArea, #available(iOS 11.0, *)
    {
        addSafeAreaConstraints(containerView: containerView, embeddedView: embeddedView)
    }
    else
    {
        activateFullViewStretchConstraints(view: embeddedView, relativeView: containerView)
    }
}

extension UIViewController
{
    func createFullViewStretchContainerView() -> UIView
    {
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(containerView)
        
        activateFullViewStretchConstraints(view: containerView, relativeView: view)
        
        return containerView
    }
    
    func addToContainerViewAndFullStretch(_ viewController: UIViewController, _ containerView: UIView)
    {
        addChild(viewController)
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(viewController.view)
        
        activateFullViewStretchConstraints(view: viewController.view, relativeView: containerView)
        
        viewController.didMove(toParent: self)
    }
    
    func embedViewController(_ viewController: UIViewController)
    {
        addToContainerViewAndFullStretch(viewController, self.view)
    }
    
    func embedViewControllerRelativeToSafeArea(_ child: UIViewController)
    {
        if #available(iOS 11.0, *)
        {
            addChild(child)

            child.view.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(child.view)

            addSafeAreaConstraints(containerView: self.view, embeddedView: child.view)

            child.didMove(toParent: self)
        }
        else
        {
            embedViewController(child)
        }
    }
}

fileprivate func addSafeAreaConstraints(containerView: UIView, embeddedView: UIView)
{
    if #available(iOS 11.0, *)
    {
        NSLayoutConstraint.activate(
        [
            embeddedView.leadingAnchor .constraint(equalTo: containerView.safeAreaLayoutGuide.leadingAnchor),
            embeddedView.trailingAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.trailingAnchor),
            embeddedView.topAnchor     .constraint(equalTo: containerView.safeAreaLayoutGuide.topAnchor),
            embeddedView.bottomAnchor  .constraint(equalTo: containerView.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    else
    {
        fatalError()
    }
}
