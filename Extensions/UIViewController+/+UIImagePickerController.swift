//
//  UIViewController+UIImagePickerController.swift
//  Sellel
//
//  Created by Dmitrii Morozov on 24/10/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

// MARK:- UIImagePickerController
extension UIViewController
{
    func createImagePickerController(_ sourceType: UIImagePickerController.SourceType) -> UIImagePickerController?
    {
        if let mediaTypes = UIImagePickerController.availableMediaTypes(for: sourceType)
        {
            let imagePicker = UIImagePickerController()
            
            imagePicker.mediaTypes = mediaTypes
            imagePicker.allowsEditing = false
            imagePicker.sourceType = sourceType
            
            return imagePicker
        }
        
        return nil
    }
}
