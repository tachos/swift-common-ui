//
//  UIViewController+ActionSheet.swift
//  Sellel
//
//  Created by Ivan Goremykin on 26.09.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

struct ActionSheetItem
{
    let title: String
    let style: UIAlertAction.Style
    let callback: VoidCallback
}

extension UIViewController
{
    func actionSheet(_ actionSheetItems: [ActionSheetItem])
    {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        (actionSheetItems + [cancelAlertAction])
            .map { UIAlertAction(title: $0.title, style: $0.style, handler: s($0.callback)) }
            .forEach(alertController.addAction)
        
        self.navigationController?.present(alertController, animated: true, completion: nil)
    }
}

private let cancelAlertAction = ActionSheetItem(title: "Cancel", style: .cancel, callback: {})

// OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE
// OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE
// OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE
// OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE
// OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE / OBSOLETE

typealias ActionSheetAction<Item> = (name: String, style: UIAlertAction.Style, item: Item, callback: (Item) -> Void)

extension UIViewController
{
    func actionSheet<Item>(title: String? = nil,
                           message: String? = nil,
                           actionItems: [ActionSheetAction<Item>])
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        for actionItem in actionItems
        {
            alertController.addAction(UIAlertAction(title: actionItem.name,
                                                    style: actionItem.style,
                                                    handler: { _ in actionItem.callback(actionItem.item) }))
        }
        
        alertController.addAction(UIAlertAction(title: localize("Cancel"), style: .cancel, handler: { (action) -> Void in }))
        
        self.navigationController!.present(alertController, animated: true, completion: nil)
    }
}
