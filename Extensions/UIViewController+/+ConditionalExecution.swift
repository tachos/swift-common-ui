//
//  UIViewController+ConditionalExecution.swift
//  StarHub
//
//  Created by Ivan Goremykin on 20.06.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

extension UIViewController
{
    func executeIfNotNil(optional: Optional<Any>, nilMessage: String, notNilCallback: () -> Void)
    {
        if optional == nil
        {
            alert(title: "Error", message: nilMessage)
        }
        else
        {
            notNilCallback()
        }
    }
    
    func executeIfNotEmpty(string: String, emptyMessage: String, notEmptyCallback: () -> Void)
    {
        executeIf(optional: string,
                  isErrorPredicate: { $0.isEmpty },
                  errorMessage: emptyMessage,
                  notErrorCallback: notEmptyCallback)
    }
    
    func executeIf<Type>(optional: Type?, isErrorPredicate: (Type) -> Bool, errorMessage: String, notErrorCallback: () -> Void)
    {
        if optional == nil || isErrorPredicate(optional!)
        {
            alert(title: localize("Error"), message: errorMessage)
        }
        else
        {
            notErrorCallback()
        }
    }
}
