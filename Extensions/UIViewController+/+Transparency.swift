//  Created by Ivan Goremykin on 12/10/2018.
//  Copyright © 2018 Averia. All rights reserved.

import UIKit

extension UIViewController
{
    func setBackgroundTransparent()
    {
        view.isOpaque = false
        view.backgroundColor = .clear
    }
}
