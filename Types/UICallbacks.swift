//
//  UICallbacks.swift
//  Broadway
//
//  Created by Ivan Goremykin on 11/11/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import UIKit

typealias ViewControllerCallback = (UIViewController) -> Void
