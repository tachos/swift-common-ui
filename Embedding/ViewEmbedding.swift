//  Created by Ivan Goremykin on 21/05/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import TinyConstraints

extension UIView
{
    func embedTableView(_ style: UITableView.Style) -> UITableView
    {
        let tableView = UITableView(frame: self.bounds, style: style)
        
        addSubview(tableView)
        tableView.edgesToSuperview()
        
        return tableView
    }
    
    func embedClearTableView(_ style: UITableView.Style) -> UITableView
    {
        let tableView = embedTableView(style)
        
        tableView.backgroundColor = .clear
        tableView.separatorColor = .clear
        
        return tableView
    }
    
    func embedClearTableView(_ style: UITableView.Style, under headerView: UIView) -> UITableView
    {
        let tableView = UITableView(frame: CGRect.zero, style: style)
        
        addSubview(tableView)
        tableView.backgroundColor = .clear
        tableView.separatorColor = .clear
        
        tableView.topToBottom(of: headerView)
        tableView.leadingToSuperview()
        tableView.trailingToSuperview()
        tableView.bottomToSuperview()
        
        return tableView
    }
    
    func embedClearTableView(_ style: UITableView.Style, above bottomView: UIView) -> UITableView
    {
        let tableView = UITableView(frame: CGRect.zero, style: style)
        
        addSubview(tableView)
        tableView.backgroundColor = .clear
        tableView.separatorColor = .clear
        
        tableView.topToSuperview()
        tableView.leadingToSuperview()
        tableView.trailingToSuperview()
        tableView.bottomToTop(of: bottomView)
        
        return tableView
    }
    
    func embedTableView(_ style: UITableView.Style, under headerView: UIView) -> UITableView
    {
        let tableView = UITableView(frame: CGRect.zero, style: style)
        
        addSubview(tableView)
        
        tableView.topToBottom(of: headerView)
        tableView.leadingToSuperview()
        tableView.trailingToSuperview()
        tableView.bottomToSuperview()
        
        return tableView
    }
    
    func embedSeparatorTableView(_ style: UITableView.Style) -> UITableView
    {
        let tableView = UITableView(frame: CGRect.zero, style: style)
        tableView.backgroundColor = .clear
        
        addSubview(tableView)
        tableView.edgesToSuperview()
        
        return tableView
    }
    
    func embedSeparatorTableView(_ style: UITableView.Style, under headerView: UIView) -> UITableView
    {
        let tableView = UITableView(frame: CGRect.zero, style: style)
        tableView.backgroundColor = .clear
        
        addSubview(tableView)
        
        tableView.topToBottom(of: headerView)
        tableView.leadingToSuperview()
        tableView.trailingToSuperview()
        tableView.bottomToSuperview()
        
        return tableView
    }
    
    func embedCollectionView() -> UICollectionView
    {
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        
        addSubview(collectionView)
        collectionView.edgesToSuperview()
        
        return collectionView
    }
    
    func embedClearCollectionView() -> UICollectionView
    {
        let collectionView = embedCollectionView()
        
        collectionView.backgroundColor = .clear
        
        return collectionView
    }
    
    func embedClearCollectionView(above bottomView: UIView) -> UICollectionView
    {
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        
        addSubview(collectionView)
        
        collectionView.topToSuperview()
        collectionView.leadingToSuperview()
        collectionView.trailingToSuperview()
        collectionView.bottomToTop(of: bottomView)
        
        return collectionView
    }
    
    func embedClearCollectionView(below topView: UIView) -> UICollectionView
    {
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        
        addSubview(collectionView)
        
        collectionView.topToBottom(of: topView)
        collectionView.leadingToSuperview()
        collectionView.trailingToSuperview()
        collectionView.bottomToSuperview()
        
        return collectionView
    }
    
    func embedClearCollectionView(topView: UIView, bottomView: UIView) -> UICollectionView
    {
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        
        addSubview(collectionView)
        
        collectionView.topToBottom(of: topView)
        collectionView.leadingToSuperview()
        collectionView.trailingToSuperview()
        collectionView.bottomToTop(of: bottomView)
        
        return collectionView
    }
}

extension UIView
{
    func embed(_ viewController: UIViewController, parentViewController: UIViewController, under topView: UIView)
    {
        parentViewController.addChild(viewController)
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(viewController.view)
        
        viewController.view.topToBottom(of: topView)
        viewController.view.leadingToSuperview()
        viewController.view.trailingToSuperview()
        viewController.view.bottomToSuperview()
        
        viewController.didMove(toParent: parentViewController)
    }
    
    func emebed(_ view: UIView, below topView: UIView)
    {
        self.addSubview(view)
        
        view.topToBottom(of: topView)
        view.leadingToSuperview()
        view.trailingToSuperview()
        view.bottomToSuperview()
    }
    
    func emebed(_ view: UIView, above bottomView: UIView)
    {
        self.addSubview(view)
        
        view.topToSuperview()
        view.leadingToSuperview()
        view.trailingToSuperview()
        view.bottomToTop(of: view)
    }
    
    func embed(below topView: UIView, _ view: UIView, above bottomView: UIView)
    {
        self.addSubview(view)
        
        view.topToBottom(of: topView)
        view.leadingToSuperview()
        view.trailingToSuperview()
        view.bottomToTop(of: bottomView)
    }
}
