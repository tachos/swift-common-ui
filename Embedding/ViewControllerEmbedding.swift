//
//  ViewControllerEmbedding.swift
//  Broadway
//
//  Created by Ivan Goremykin on 29/05/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import TinyConstraints
import UIKit

extension UIViewController
{
    func embed(_ viewController: UIViewController, under topView: UIView)
    {
        addChild(viewController)
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(viewController.view)
        
        viewController.view.topToBottom(of: topView)
        viewController.view.leadingToSuperview()
        viewController.view.trailingToSuperview()
        viewController.view.bottomToSuperview()
        
        viewController.didMove(toParent: self)
    }

    func embed(_ viewController: UIViewController, above bottomView: UIView)
    {
        addChild(viewController)
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(viewController.view)

        viewController.view.topToSuperview()
        viewController.view.leadingToSuperview()
        viewController.view.trailingToSuperview()
        viewController.view.bottomToTop(of: bottomView)

        viewController.didMove(toParent: self)
    }
    
    func embed(_ child: UIViewController, verticalOffset: CGFloat, _ bottomLimitationView: UIView? = nil)
    {
        addChild(child)
        
        child.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(child.view)
        
        child.view.leadingToSuperview()
        child.view.trailingToSuperview()
        if let bottomLimitationView = bottomLimitationView
        {
            child.view.bottomToTop(of: bottomLimitationView)
        }
        else
        {
            child.view.bottomToSuperview()
        }
        child.view.topToSuperview(offset: verticalOffset)
        
        child.didMove(toParent: self)
    }
    
    func embedStickToBottom(height: CGFloat, view: UIView,
                            _ viewHasPredefinedHeight: Bool = false,
                            bottomToSafeArea: Bool = true)
    {
        self.view.addSubview(view)
        
        view.leadingToSuperview()
        view.trailingToSuperview()
        
        if !viewHasPredefinedHeight
        {
            if #available(iOS 11.0, *)
            {
                view.topToSuperview(self.view.safeAreaLayoutGuide.bottomAnchor, offset: -height, isActive: true)
            }
            else
            {
                view.topToSuperview(self.bottomLayoutGuide.topAnchor, offset: -height, isActive: true)
            }
        }
        
        view.bottomToSuperview(usingSafeArea: bottomToSafeArea)
    }    
}
