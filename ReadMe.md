# CommonUI
A bunch of UI-related utilities reused in all our projects

## What's inside?

- type safe instantiation of view controllers, views and reusable cells
- various UI controls extensions
- reused UI controls
- reused view controllers
- +
